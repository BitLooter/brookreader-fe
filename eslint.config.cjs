module.exports = {
  extends: [
    'react-app',
    'eslint:recommended',
    'plugin:eslint-comments/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:jest/recommended',
    'plugin:jest/style'
  ],
  ignorePatterns: ['**/*.d.ts'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    // Allow ESLint to check configuration files in the project root
    extraFileExtensions: ['.cjs'],
    project: './tsconfig.noexclude.json'
  },
  rules: {
    'eqeqeq': 'error',
    'key-spacing': 'error',
    // Allow console.log for debugging because I am lazy and bad at debuggers
    'no-console': 'warn',
    'no-multiple-empty-lines': 'error',
    'no-trailing-spaces': 'error',
    'no-unneeded-ternary': 'error',
    'no-var': 'error',
    'prefer-const': 'warn',
    // I will die on this hill
    'semi': [
      'error',
      'never',
      {
        beforeStatementContinuationChars: 'always'
      }
    ],
    // TODO: Change to error when all are fixed
    '@typescript-eslint/consistent-type-imports': 'warn',
    '@typescript-eslint/explicit-function-return-type': [
      'warn',
      {
        allowExpressions: true
      }
    ],
    '@typescript-eslint/member-delimiter-style': [
      'warn',
      {
        multiline: {
          delimiter: 'none'
        }
      }
    ],
    '@typescript-eslint/naming-convention': [
      'warn',
      { selector: 'memberLike', format: ['camelCase'], leadingUnderscore: 'allow' },
      { selector: 'enumMember', format: ['PascalCase'] },
      // Allow PascalCase functions for JSX components
      { selector: 'function', format: ['camelCase', 'PascalCase'] },
      // Allow PascalCase for useKeyboard bindings
      { selector: 'objectLiteralProperty', format: ['camelCase', 'PascalCase'],
        types: ['function'] },
      { selector: 'property', format: null, leadingUnderscore: 'allow',
        filter: { regex: '[- ]|Range', match: true } },
      { selector: 'typeLike', format: ['PascalCase'] },
      { selector: 'variableLike', format: ['camelCase'] },
      { selector: 'variable', format: ['camelCase', 'UPPER_CASE'] },
      { selector: 'parameter', format: ['camelCase'], leadingUnderscore: 'allow' },
    ],
    '@typescript-eslint/no-empty-function': 'off'
  }
}
