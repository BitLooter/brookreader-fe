import * as React from 'react'

import DEFAULT_COVER from '../images/default_cover.png'
import DEFAULT_SHELF_COVER from '../images/default_shelf_cover.png'

export default MediaCoverImg


const IMAGE_STYLE: React.CSSProperties = {
  color: 'transparent',
  objectFit: 'contain'
}

interface MediaCoverProps extends React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement> {
  catagory: 'book' | 'shelf'
}

/** Renders an <img> with a fallback to a default cover on load errors. Image
 * will scale to img size but maintain aspect ratio.
 *
 * Takes the same props as a regular <img> tag but overrides alt, style, and
 * onError with its own.
 *
 * @param coverUrl  URL to use as cover image
 */
function MediaCoverImg(props: MediaCoverProps): JSX.Element {
  let altText
  let coverUrl: string
  switch (props.catagory) {
    case 'book':
      altText = 'Cover of book'
      coverUrl = DEFAULT_COVER
      break
    case 'shelf':
      altText = 'Cover of shelf'
      coverUrl = DEFAULT_SHELF_COVER
      break
    default:
      throw new Error('Unknown media type for cover')
  }

  return (
    <img
      {...props}
      alt={altText}
      style={IMAGE_STYLE}
      onError={ (e) => e.currentTarget.src = coverUrl}
    />
  )
}
