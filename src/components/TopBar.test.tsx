import React from 'react'

import { renderWithRouter } from '../testutils'

import TopBar from './TopBar'


test('TopBar renders', () => {
  renderWithRouter(<TopBar backUrl='https://any.where/' title='Test' />)
})
