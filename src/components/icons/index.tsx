/** SVG icons used by the application */
// Structured as a folder module to allow for code splitting or external files later.

import * as React from 'react'


// https://materialdesignicons.com/icon/arrow-left
const arrowLeftPath = <path fill="#000000" d="M20,11V13H8L13.5,18.5L12.08,19.92L4.16,12L12.08,4.08L13.5,5.5L8,11H20Z" />
// https://materialdesignicons.com/icon/book-multiple
const bookMultiple = <path fill="#000000" d="M19,18H9A2,2 0 0,1 7,16V4A2,2 0 0,1 9,2H10V7L12,5.5L14,7V2H19A2,2 0 0,1 21,4V16A2,2 0 0,1 19,18M17,20V22H5A2,2 0 0,1 3,20V6H5V20H17Z" />
// https://materialdesignicons.com/icon/book-open-variant
const bookOpenVariantPath = <path fill="#000000" d="M21,5C19.89,4.65 18.67,4.5 17.5,4.5C15.55,4.5 13.45,4.9 12,6C10.55,4.9 8.45,4.5 6.5,4.5C4.55,4.5 2.45,4.9 1,6V20.65C1,20.9 1.25,21.15 1.5,21.15C1.6,21.15 1.65,21.1 1.75,21.1C3.1,20.45 5.05,20 6.5,20C8.45,20 10.55,20.4 12,21.5C13.35,20.65 15.8,20 17.5,20C19.15,20 20.85,20.3 22.25,21.05C22.35,21.1 22.4,21.1 22.5,21.1C22.75,21.1 23,20.85 23,20.6V6C22.4,5.55 21.75,5.25 21,5M21,18.5C19.9,18.15 18.7,18 17.5,18C15.8,18 13.35,18.65 12,19.5V8C13.35,7.15 15.8,6.5 17.5,6.5C18.7,6.5 19.9,6.65 21,7V18.5Z" />
// https://materialdesignicons.com/icon/cog
const cogPath = <path fill="currentColor" d="M12,15.5A3.5,3.5 0 0,1 8.5,12A3.5,3.5 0 0,1 12,8.5A3.5,3.5 0 0,1 15.5,12A3.5,3.5 0 0,1 12,15.5M19.43,12.97C19.47,12.65 19.5,12.33 19.5,12C19.5,11.67 19.47,11.34 19.43,11L21.54,9.37C21.73,9.22 21.78,8.95 21.66,8.73L19.66,5.27C19.54,5.05 19.27,4.96 19.05,5.05L16.56,6.05C16.04,5.66 15.5,5.32 14.87,5.07L14.5,2.42C14.46,2.18 14.25,2 14,2H10C9.75,2 9.54,2.18 9.5,2.42L9.13,5.07C8.5,5.32 7.96,5.66 7.44,6.05L4.95,5.05C4.73,4.96 4.46,5.05 4.34,5.27L2.34,8.73C2.21,8.95 2.27,9.22 2.46,9.37L4.57,11C4.53,11.34 4.5,11.67 4.5,12C4.5,12.33 4.53,12.65 4.57,12.97L2.46,14.63C2.27,14.78 2.21,15.05 2.34,15.27L4.34,18.73C4.46,18.95 4.73,19.03 4.95,18.95L7.44,17.94C7.96,18.34 8.5,18.68 9.13,18.93L9.5,21.58C9.54,21.82 9.75,22 10,22H14C14.25,22 14.46,21.82 14.5,21.58L14.87,18.93C15.5,18.67 16.04,18.34 16.56,17.94L19.05,18.95C19.27,19.03 19.54,18.95 19.66,18.73L21.66,15.27C21.78,15.05 21.73,14.78 21.54,14.63L19.43,12.97Z" />
// https://materialdesignicons.com/icon/download
const downloadPath = <path fill="#000000" d="M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z" />
// https://materialdesignicons.com/icon/file-download
const fileDownloadPath = <path fill="#000000" d="M14,2H6C4.89,2 4,2.89 4,4V20C4,21.11 4.89,22 6,22H18C19.11,22 20,21.11 20,20V8L14,2M12,19L8,15H10.5V12H13.5V15H16L12,19M13,9V3.5L18.5,9H13Z" />
// https://materialdesignicons.com/icon/folder-download
const folderDownloadPath = <path fill="#000000" d="M20,6A2,2 0 0,1 22,8V18A2,2 0 0,1 20,20H4C2.89,20 2,19.1 2,18V6C2,4.89 2.89,4 4,4H10L12,6H20M19.25,13H16V9H14V13H10.75L15,17.25" />
// https://materialdesignicons.com/icon/fullscreen
const fullscreenPath = <path fill="#000000" d="M5,5H10V7H7V10H5V5M14,5H19V10H17V7H14V5M17,14H19V19H14V17H17V14M10,17V19H5V14H7V17H10Z" />
// https://materialdesignicons.com/icon/fullscreen-exit
const fullscreenExitPath = <path fill="#000000" d="M14,14H19V16H16V19H14V14M5,14H10V19H8V16H5V14M8,5H10V10H5V8H8V5M19,8V10H14V5H16V8H19Z" />
// https://materialdesignicons.com/icon/information
const informationPath = <path fill="#000000" d="M13,9H11V7H13M13,17H11V11H13M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2Z" />
// https://materialdesignicons.com/icon/keyboard-variant
const keyboardPath = <path fill="#000000" d="M6,16H18V18H6V16M6,13V15H2V13H6M7,15V13H10V15H7M11,15V13H13V15H11M14,15V13H17V15H14M18,15V13H22V15H18M2,10H5V12H2V10M19,12V10H22V12H19M18,12H16V10H18V12M8,12H6V10H8V12M12,12H9V10H12V12M15,12H13V10H15V12M2,9V7H4V9H2M5,9V7H7V9H5M8,9V7H10V9H8M11,9V7H13V9H11M14,9V7H16V9H14M17,9V7H22V9H17Z" />
// https://materialdesignicons.com/icon/home
const homePath = <path fill="#000000" d="M10,20V14H14V20H19V12H22L12,3L2,12H5V20H10Z" />

function makeSvgComponent(path: JSX.Element) {
  return (props: React.SVGProps<SVGSVGElement>) => <svg {...props} viewBox="0 0 24 24">
    {path}
  </svg>
}

// Icons are JSX components and should have PascalCase names
/* eslint-disable @typescript-eslint/naming-convention */

export const ArrowLeftIcon = makeSvgComponent(arrowLeftPath)
export const BookMultipleIcon = makeSvgComponent(bookMultiple)
export const BookOpenVariantIcon = makeSvgComponent(bookOpenVariantPath)
export const CogIcon = makeSvgComponent(cogPath)
export const DownloadIcon = makeSvgComponent(downloadPath)
export const FileDownloadIcon = makeSvgComponent(fileDownloadPath)
export const FolderDownloadIcon = makeSvgComponent(folderDownloadPath)
export const FullscreenIcon = makeSvgComponent(fullscreenPath)
export const FullscreenExitIcon = makeSvgComponent(fullscreenExitPath)
export const InformationIcon = makeSvgComponent(informationPath)
export const KeyboardIcon = makeSvgComponent(keyboardPath)
export const HomeIcon = makeSvgComponent(homePath)

/* eslint-enable @typescript-eslint/naming-convention */
