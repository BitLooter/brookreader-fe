import React from 'react'

import { Link } from 'react-router-dom'

import log from '../logger'
import { UserCommandProperties } from '../models'


/** A single menu item for use with OverlayMenu
 *
 * @param label  Text of menu item
 * @param icon  Optional JSX element, typically an img or svg, to be used as an icon
 *
 * Only one of the following parameters may be used:
 * @param action  Callback function called when item is selected
 * @param route  Internal route to link
 * @param url  External URL to link
 */
function OverlayMenuItem({label, icon, action, url, route}: UserCommandProperties): JSX.Element {
  const linkContents = <span>{label}</span>
  const actionWithoutPropagate = (event: React.MouseEvent): void => {
    if (action) {
      action()
    }
    event.stopPropagation()
  }
  const menuItem = <div className='overlayMenuItem noBackdropAction'>
    {icon} {label}
  </div>

  let link: JSX.Element
  if (action) {
    link = <span onClick={actionWithoutPropagate}>{menuItem}</span>
  } else if (route) {
    link = <Link to={route}>{menuItem}</Link>
  } else if (url) {
    link = <a href={url}>{menuItem}</a>
  } else {
    log.warning(`No target set for menu item "${label}"`)
    link = <span>{linkContents}</span>
  }

  return link
}

export default OverlayMenuItem
