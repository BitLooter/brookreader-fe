/** Component for displaying book's options menu on an overlay */

import * as React from 'react'

import { UserSettingsContext } from '../context'
import LinkButton from './LinkButton'
import ToggleSwitch from './ToggleSwitch'
import './OverlaySettingsMenu.css'


export default OverlaySettingsMenu


interface OverlaySettingsMenuProps {
  closeSettings: () => void
}

function OverlaySettingsMenu({closeSettings}: OverlaySettingsMenuProps): JSX.Element {
  const settings = React.useContext(UserSettingsContext)

  return <div className='overlaySettingsMenuContainer'>
    <LinkButton label='Go back to main menu' action={closeSettings} />
    <ul>
      <li>
        <ToggleSwitch
          initialState={settings.bookAnimatedTransitions}
          label='Animated page transitions:'
          onToggle={(v) => settings.updateSetting('bookAnimatedTransitions', v)}
        />
      </li>
      <li>
        <ToggleSwitch
          initialState={settings.bookProgressVisible}
          label='Show book progress bar'
          onToggle={(v) => settings.updateSetting('bookProgressVisible', v)}
        />
      </li>
    </ul>
  </div>
}
