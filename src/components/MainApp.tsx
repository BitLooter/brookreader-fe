import React from 'react'

import { TopBarContext } from '../context'
import './MainApp.css'
import TopBar, { TopBarIcon } from './TopBar'


export interface BarState {
  backUrl: string | null
  icons: TopBarIcon[]
  overlay: boolean
  title: string
  hidden: boolean
}

interface BarActionSetHidden {
  type: 'sethidden'
  hidden: boolean
}
interface BarActionSetNav {
  type: 'setnav'
  backUrl: string
  title: string
}
interface BarActionSetOverlay {
  type: 'setoverlay'
  overlay: boolean
}
interface BarActionSetup {
  type: 'setup'
  newState: BarState
}
type BarAction = BarActionSetHidden | BarActionSetNav | BarActionSetOverlay | BarActionSetup

export const BARACTION_SETHIDDEN = 'sethidden'
export const BARACTION_SETNAV = 'setnav'
export const BARACTION_SETOVERLAY = 'setoverlay'
export const BARACTION_SETUP = 'setup'

function barReducer(state: BarState, action: BarAction): BarState {
  switch (action.type) {
    case BARACTION_SETHIDDEN:
      return {...state, hidden: action.hidden}
    case BARACTION_SETNAV:
      return {...state, backUrl: action.backUrl, title: action.title}
    case BARACTION_SETOVERLAY:
      return {...state, overlay: action.overlay}
    case BARACTION_SETUP:
      return action.newState
    default:
      throw new Error('Unknown BARACTION in reducer')
  }
}
const defaultBarState: BarState = {
  backUrl: '',
  overlay: false,
  icons: [],
  title: '',
  hidden: false
}

interface MainAppProps {
  children: React.ReactNode | React.ReactNode[]
}

/** Wrap app contents with common UI elements */
function MainApp({children}: MainAppProps): JSX.Element {
  const [barState, barDispatch] = React.useReducer(barReducer, defaultBarState)

  // Set up commands to control top bar
  const barContext = {
    setNav: (backUrl: string, title: string) => {
      if (barState.backUrl !== backUrl || barState.title !== title) {
        barDispatch({type: BARACTION_SETNAV, backUrl, title})
      }
    },
    setOverlay: (overlay: boolean) => {
      if (barState.overlay !== overlay) {
        barDispatch({type: BARACTION_SETOVERLAY, overlay})
      }
    },
    setHidden: (hidden: boolean) => {
      if (barState.hidden !== hidden) {
        barDispatch({type: BARACTION_SETHIDDEN, hidden})
      }
    },
    setup: (newState: BarState) => {
      barDispatch({type: BARACTION_SETUP, newState})
    },
  }

  let mainClass = ''
  if (!barState.overlay) {
    mainClass = 'withTopBar'
  }

  // NOTE: TopBar works OK like this but if we ever want to reuse it outside of
  //  MainApp it should pull all the context-handling code into TopBar and make
  //  main a child element of <TopBar />.
  return <>
    <TopBar
      backUrl={barState.backUrl}
      hidden={barState.hidden}
      icons={barState.icons}
      overlay={barState.overlay}
      title={barState.title}
    />
    <main className={mainClass}>
      <TopBarContext.Provider value={barContext}>
        {children}
      </TopBarContext.Provider>
    </main>
  </>
}

export default MainApp
