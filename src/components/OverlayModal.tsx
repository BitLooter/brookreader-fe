import * as React from 'react'

import log from '../logger'

import './OverlayModal.css'


export default OverlayModal

interface OverlayModalProps {
  backdropAction?: () => void
  children: React.ReactNode | React.ReactNode[]
  className?: string
}

/** Dims window contents to draw attention to child elements.
 *
 * @param className: A CSS class to apply to the wrapper element in addition to
 *  the default "overlayModal".
*/
function OverlayModal({backdropAction, children, className}: OverlayModalProps): JSX.Element {
  /** Check that clicked element does not have 'noBackdropAction' class before
   * calling backdropAction
   */
  let backdropActionChecked
  if (backdropAction) {
    backdropActionChecked = (e: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
      if ((e.target as Element).closest('.noBackdropAction') === null) {
        log.debug('Calling backdrop action')
        backdropAction()
      }
    }
  }

  return <div className={'overlayModal ' + className} onClick={backdropActionChecked}>
    {children}
  </div>
}
