import React from 'react'

import useTopBar from './hooks/usetopbar'
import LoadingMessage from './LoadingMessage'

export default LoadingScreen


interface LoadingScreenProps {
  backRoute: string
  message: string
  title: string
}

/** Loading message intended to fill the entire screen, e.g. changing the
 * TopBar as well
 *
 * @param backRoute  URL to redirect on navigating back
 * @param message  Loading message
 * @param title  Title of loading screen, Displayed in topbar
 */
function LoadingScreen({backRoute, message, title}: LoadingScreenProps): JSX.Element {
  useTopBar(backRoute, title)

  return <LoadingMessage message={message} />
}
