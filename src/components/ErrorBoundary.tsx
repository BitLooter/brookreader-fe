import React from 'react'
import { Link } from 'react-router-dom'

import { TopBarContext } from '../context'
import log from '../logger'

import './ErrorBoundary.css'


interface ErrorBoundaryProps {
  /** URL to redirect to on closing boundary */
  backUrl: string
  /** Help information displayed below title */
  info: string
  /** Error title */
  title: string
  /** Short version of title (e.g. for use in topbar) */
  titleShort: string
}

/** Catch and display unrecoverable errors, displaying details and offering
 * help information and links.
 *
 * Currently error boundaries still require class-based components.
 */
class ErrorBoundary extends React.Component<ErrorBoundaryProps, {error?: Error}> {
  static contextType = TopBarContext
  // TODO:TS: See docstring for this.context in TS 3.7, CRA still doesn't work
  // https://github.com/facebook/create-react-app/issues/8918
  context!: React.ContextType<typeof TopBarContext>

  public static getDerivedStateFromError(error: Error): {error: Error} {
    return {error}
  }

  constructor(props: ErrorBoundaryProps) {
    super(props)
    this.state = {}
  }

  public componentDidCatch(error: Error, info: React.ErrorInfo): void {
    log.error('Error rendering book: ' + error.message)
  }

  public render(): JSX.Element | React.ReactNode {
    if (this.state.error) {
      const {backUrl, info, title, titleShort} = this.props
      this.context.setNav(backUrl, titleShort)
      return <div className='errorBoundaryContainer'>
        <h1>{title}</h1>
        <p>{info}</p>
        <h2><Link to={backUrl} className='errorBoundaryLink'>Take me back home</Link></h2>
        <h2>The ugly details:</h2>
        {this.state.error.message}
      </div>
    }
    return this.props.children
  }
}

export default ErrorBoundary
