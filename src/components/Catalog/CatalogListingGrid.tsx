import React from 'react'

import { CatalogCollection } from '../../models'
import * as routes from '../../routes'

import CatalogCover from './CatalogCover'


interface CatalogListingGridProps {
  bookUrlBuilder?: (collectionId: string, bookId: string) => string
  collection: CatalogCollection
  collectionUrlBuilder?: (collectionId: string) => string
}

/** Displays items in a collection, as a cover grid
 * @param bookUrlBuilder- Generates URL for a book from IDs of collection and book
 * @param collection - Collection object with data to render
 * @param collectionUrlBuilder - Function to generate an URL from a collection ID
 */
function CatalogListingGrid(
  {bookUrlBuilder, collection, collectionUrlBuilder}: CatalogListingGridProps
): JSX.Element {
  const buildBookUrl = bookUrlBuilder ?? routes.makeBookDefaultPageUrl
  const buildCollectionUrl = collectionUrlBuilder ?? routes.makeCatalogUrl

  const listElements: JSX.Element[] = []
  for (const sub of collection.collections.values()) {
    listElements.push(
      <CatalogCover
        count={sub.count}
        coverUrl={sub.coverUrl}
        label={sub.name}
        targetUrl={buildCollectionUrl(sub.id)}
        key={sub.id}
      />
    )
  }
  collection.books.forEach( (book, bookId) => listElements.push(
    <CatalogCover
      coverUrl={book.coverThumbUrl}
      label={book.title}
      targetUrl={buildBookUrl(collection.id, book.id)}
      key={bookId}
    />
  ))

  return <div className='catalogListingCommon catalogListingGrid'>
    {listElements}
  </div>
}

export default CatalogListingGrid
