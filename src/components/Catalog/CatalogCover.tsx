import * as React from 'react'
import { useInView } from 'react-intersection-observer'
import { Link } from 'react-router-dom'

import MediaCoverImg from '../MediaCoverImg'
import { BookMultipleIcon } from '../icons'
import './CatalogCover.css'

export default CatalogCover


interface CatalogCoverProps {
  count?: number
  coverUrl: string
  label: string
  targetUrl: string
}

function CatalogCover({count, coverUrl, label, targetUrl}: CatalogCoverProps): JSX.Element {
  const [coverRef, coverVisible] = useInView()

  const countAnnotation = count ?
    <div className='catalogCoverCount'><BookMultipleIcon /> {count}</div> :
    null

  // Only render the cover if it's in the browser viewport
  let coverContents
  if (coverVisible) {
    coverContents = <Link to={targetUrl}>
      <div className='catalogCover'>
        <MediaCoverImg src={coverUrl} catagory='shelf' />
        <div className='catalogCoverTextBackground'>
          <div className='catalogCoverText'>{label}</div>
        </div>
        {countAnnotation}
      </div>
    </Link>
  }

  return <div className='catalogCoverContainer' ref={coverRef}>
    {coverContents}
  </div>
}
