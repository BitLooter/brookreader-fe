import React from 'react'

import { CatalogCollection } from '../../models'
import * as routes from '../../routes'

import CatalogListingGrid from './CatalogListingGrid'
import './Catalog.css'


export default Catalog

interface CatalogProps {
  collection: CatalogCollection
  isShowcase: boolean
}

/** Present books available in the given collection
 *
 * @param collection: Collection object to render
 * @param isShowcase: Set true if it should behave like a showcase
*/
function Catalog({collection, isShowcase}: CatalogProps): JSX.Element {
  let bookUrlBuilder
  if (isShowcase) {
    const collectionKey = routes.buildCollectionKey(collection.id, 'showcase')
    bookUrlBuilder = (_: string, id: string) => routes.makeBookDefaultPageUrl(collectionKey, id)
  }
  const collectionUrlBuilder = isShowcase ? routes.makeShowcaseUrl : routes.makeCatalogUrl

  return <div className='catalogContainer'>
    <CatalogListingGrid
      bookUrlBuilder={bookUrlBuilder}
      collectionUrlBuilder={collectionUrlBuilder}
      collection={collection}
    />
  </div>
}
