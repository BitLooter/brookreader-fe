import React from 'react'
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils'

import {
  MOCK_CATALOG_URL, MOCK_COLLECTION, MOCK_COLLECTION_ID
} from '../../testdata'
import { renderWithRouter, renderWithTopBar } from '../../testutils'
import CatalogContainer from './Catalog.container'
import CatalogCover from './CatalogCover'
import CatalogListingGrid from './CatalogListingGrid'


jest.mock('../hooks/usecatalogindex')

test('Catalog container tests', () => {
  renderWithTopBar(
    <CatalogContainer collectionKey={MOCK_COLLECTION_ID} />,
    {url: MOCK_CATALOG_URL}
  )
})
test.todo('Catalog with empty index')

describe('Catalog component smoke tests', () => {
  test('CatalogCover renders', () => {
    renderWithRouter(
      <CatalogCover coverUrl='Cover URL' targetUrl='Target URL' label='Cover' />,
      MOCK_CATALOG_URL
    )
  })

  test('CatalogListing renders', () => {
    mockAllIsIntersecting(false)
    renderWithRouter(
      <CatalogListingGrid collection={MOCK_COLLECTION} />,
      MOCK_CATALOG_URL
    )
  })
})
