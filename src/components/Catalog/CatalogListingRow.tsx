import React from 'react'

import { CatalogCollection } from '../../models'
import * as routes from '../../routes'

import CatalogCover from './CatalogCover'


interface CatalogListingGridProps {
  bookUrlBuilder?: (collectionId: string, bookId: string) => string
  collection: CatalogCollection
  collectionUrlBuilder?: (collectionId: string) => string
}

/** Displays items in a collection, as a single row.
 * @param bookUrlBuilder- Generates URL for a book from IDs of collection and book
 * @param collection - Collection object with data to render
 * @param collectionUrlBuilder - Function to generate an URL from a collection ID
 */
function CatalogListingRow(
  {bookUrlBuilder, collection, collectionUrlBuilder}: CatalogListingGridProps
): JSX.Element {
  const buildBookUrl = bookUrlBuilder ?? routes.makeBookUrl
  const buildCollectionUrl = collectionUrlBuilder ?? routes.makeCatalogUrl

  // CatalogCover will only render when cover is visible, dozens of covers
  // pushing off the side of the screen shouldn't be a problem
  const listElements: JSX.Element[] = []
  for (const sub of collection.collections.values()) {
    listElements.push(
      <CatalogCover
        count={sub.count}
        coverUrl={sub.coverUrl}
        label={sub.name}
        targetUrl={buildCollectionUrl(sub.id)}
        key={sub.id}
      />
    )
  }
  for (const [bookId, book] of collection.books) {
    listElements.push(
      <CatalogCover
        coverUrl={book.coverThumbUrl}
        label={book.title}
        targetUrl={buildBookUrl(collection.id, book.id)}
        key={bookId}
      />
    )
  }

  return <div className='catalogListingCommon catalogListingRow'>
    {listElements}
  </div>
}

export default CatalogListingRow
