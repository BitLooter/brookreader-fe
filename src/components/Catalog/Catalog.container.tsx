/** Container component for catalog view */

import React from 'react'
import { useHistory } from 'react-router-dom'

import useCollection from '../hooks/usecollection'
import useTopBar from '../hooks/usetopbar'
import { DEFAULT_ROOT_SHELF } from '../../config'
import { makeLogger } from '../../logger'
import * as routes from '../../routes'
import { setTitle } from '../../util'

import Catalog from './Catalog'
import LinkButton from '../LinkButton'
import LoadingScreen from '../LoadingScreen'


const log = makeLogger('CatalogContainer')

const CATALOG_LOADING_SCREEN = <LoadingScreen
  backRoute={routes.homeUrl}
  message='LOADING CATALOG...'
  title='Loading catalog'
/>
const SHOWCASE_ID_FEATURED = 'featured'
const SHOWCASE_ID_NEW = 'recent'


interface CatalogProps {
  collectionKey: string
  isShowcase?: boolean
}

/** Top-level container for displaying a media catalog
 *
 * @param collectionKey  ID of the collection to display. Use rootCollection in JSON if undefined.
 * @param isShowcase  When true modifies output for showcase collections
*/
export default function CatalogContainer({collectionKey, isShowcase}: CatalogProps): JSX.Element {
  const {setNav} = useTopBar(routes.catalogUrl, 'Loading catalog...')
  const [collectionStatus, collectionId] = useCollection(collectionKey)
  React.useEffect( () => {
    setTitle('Loading index')
    return setTitle
  }, [])

  log.debug('Catalog collection ' + collectionKey)

  // Loading/error messages
  if (collectionStatus.status === 'error') {
    throw collectionStatus.error
  } else if (collectionStatus.status === 'uninitialized' || collectionStatus.status === 'loading') {
    log.info('Loading book index')
    return CATALOG_LOADING_SCREEN
  }

  let collection
  if (collectionStatus.status === 'ready') {
    if (collectionStatus.response === undefined) {
      setNav(routes.homeUrl, 'Collection not found')
      return <Collection404 collectionId={collectionId ? collectionId : '<undefined>'} />
    } else {
      collection = collectionStatus.response
    }
  } else {
    // Type guard against unknown values for status
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    throw new Error(`Unknown shelfStatus value ${(collectionStatus as any).status}`)
  }

  let backLink
  let title
  if (isShowcase) {
    switch (collectionId) {
      case SHOWCASE_ID_FEATURED:
        title = 'Featured'
        break
      case SHOWCASE_ID_NEW:
        title = 'Recently added'
        break
      default:
        title = 'Featured: ' + collection.name
    }
    backLink = routes.homeUrl
  } else {
    title = collection.name
    // Ternary operators can't be wrong when they feel so right
    backLink = collection.parent ?
      collection.parent.id === DEFAULT_ROOT_SHELF ?
        routes.catalogUrl :
        routes.makeCatalogUrl(collection.parent.id) :
      routes.homeUrl
  }
  setNav(backLink, title)
  setTitle(title)
  return <Catalog collection={collection} isShowcase={isShowcase === true} />
}

/** Renders an error for when collectionId not present in index */
function Collection404({collectionId}: {collectionId: string}): JSX.Element {
  const history = useHistory()
  return <div className='catalog404Container'>
    <div className='catalog404Highlight'>
      <h1>Unable to find "{collectionId}"</h1>
      <p>The collection with the ID "{collectionId}" was not found. It may have
        been moved or deleted, or the URL is invalid.</p>
    </div>
    <LinkButton label='Take me back' action={history.goBack} />
    <LinkButton label='Go home' route={routes.homeUrl} />
  </div>
}
