/** Component for rendering an error message in place of a page */

import React from 'react'

import './BookPageError.css'

export default BookPageError


interface BookPageErrorProps {
  message: string
  pageNum: number
}

/** Renders an error message
 *
 * @param message  Error message to display
 * @param pageNum  Page error occured on. Zero-indexed page numbers corrected
 *  to one-indexed when displayed to user.
 */
function BookPageError({message, pageNum}: BookPageErrorProps): JSX.Element {
  return <div className='bookPageErrorContainer'>
    <h1>Could not display page {pageNum + 1}</h1>
    <p>
      This could be a temporary error, or a problem with the book source.
      Reload the page or try again later.
    </p>
    <p>
      Details: <span className='bookPageErrorDetails'>{message}</span>
    </p>
  </div>
}
