/** Component for main book menu alongside other widgets */

import * as React from 'react'

import { BookViewerContext, UserSettingsContext } from '../../context'
import useKeyboard from '../hooks/usekeyboard'
import { UserCommandProperties } from '../../models'

import * as icons from '../icons'
import MediaCoverImg from '../MediaCoverImg'
import OverlayMenu from '../OverlayMenu'
import ToggleSwitch from '../ToggleSwitch'
import BookProgress from './BookProgress'
import './BookMenu.css'


interface BookMenuProps {
  hideMenu: () => void
  closeRoute: string
  showInputSettings: () => void
  showUserSettings: () => void
}

/** Display the main book menu along with other information about the current book
 *
 * @param hideMenu: Function that closes the menu
 * @param closeRoute: Route to redirect to close the book
 */
function BookMenu({hideMenu, closeRoute, showInputSettings, showUserSettings}: BookMenuProps): JSX.Element {
  const {closeBook, details, pageNum, reader} = React.useContext(BookViewerContext)
  const {bookProgressVisible, updateSetting} = React.useContext(UserSettingsContext)
  useKeyboard({
    Escape: hideMenu,
    q: closeBook,
  })

  async function downloadPage(): Promise<void> {
    const downloadElement = document.getElementById('downloadElement') as HTMLElement
    const dataUrl = await reader.getPageUrl(pageNum)
    const dataExt = reader.getPageInfo(pageNum).extension
    downloadElement.setAttribute('href', dataUrl)
    const pageNumPadding = '00'.substr(String(pageNum).length - 1)
    downloadElement.setAttribute('download', `${details.title} pg${pageNumPadding}${pageNum}.${dataExt}`)
    downloadElement.click()
  }

  const menuOptions: UserCommandProperties[] = [
    {label: 'Back to reading', icon: <icons.BookOpenVariantIcon />, action: hideMenu},
    {label: 'Close book', icon: <icons.ArrowLeftIcon />, route: closeRoute},
    {label: 'Save this page', icon: <icons.FileDownloadIcon />, action: downloadPage},
    {label: 'Controls help', icon: <icons.KeyboardIcon />, action: showInputSettings},
    {label: 'Settings', icon: <icons.CogIcon />, action: showUserSettings},
  ]

  let downloadDetail
  if (reader.archiveType !== 'dir') {
    downloadDetail = <li className='viewerMenuDetailsDownload noBackdropAction'>
      <a href={details.url}>Download source book <icons.DownloadIcon /></a>
    </li>
  }

  return <div className='bookMenuDashboard'>
    <OverlayMenu items={menuOptions} />
    <MediaCoverImg
      className='bookMenuCover noBackdropAction'
      src={details.coverUrl}
      catagory='book'
    />
    <ul className='bookMenuDetails'>
      <li className='bookMenuDetailsTitle noBackdropAction'>{details.title}</li>
      <li className='bookMenuDetailsAdded noBackdropAction'>
        Added on {details.dateAdded.toString()}
      </li>
      {downloadDetail}
    </ul>
    <div className='bookMenuProgressToggle noBackdropAction'>
      <ToggleSwitch
        initialState={bookProgressVisible}
        label='Show progress over book:'
        onToggle={(v) => updateSetting('bookProgressVisible', v)}
      />
    </div>
    <BookProgress big={true} beforeJump={hideMenu} />
  </div>
}

export default BookMenu
