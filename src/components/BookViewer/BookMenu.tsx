/** Component for displaying book's options menu */

import * as React from 'react'

import BookMenuDashboard from './BookMenuDashboard'
import LinkButton from '../LinkButton'
import OverlayModal from '../OverlayModal'
import OverlaySettingsMenu from '../OverlaySettingsMenu'
import './BookMenu.css'


export default BookMenu

enum Submenus {
  dashboard,
  inputHelp,
  settings
}

interface BookMenuProps {
  hideMenu: () => void
  closeRoute: string
}

/** Display a menu overlaying the window contents with options relating to the
 * current book.
 *
 * @param hideMenu: Function that closes the menu
 * @param closeRoute: Route to redirect to close the book
 */
function BookMenu({hideMenu, closeRoute}: BookMenuProps): JSX.Element {
  const [currentMenu, setCurrentMenu] = React.useState(Submenus.dashboard)
  const setMenuDashboard = (): void => void setCurrentMenu(Submenus.dashboard)

  let menu
  if (currentMenu === Submenus.dashboard) {
    menu = <BookMenuDashboard
      hideMenu={hideMenu}
      closeRoute={closeRoute}
      showInputSettings={() => setCurrentMenu(Submenus.inputHelp)}
      showUserSettings={() => setCurrentMenu(Submenus.settings)}
    />
  } else if (currentMenu === Submenus.inputHelp) {
    // Show keyboard controls (and gamepad controls when implemented)
    menu = <BookKeyboardHelp closeHelp={setMenuDashboard} />
  } else if (currentMenu === Submenus.settings) {
    menu = <OverlaySettingsMenu closeSettings={setMenuDashboard} />
  } else {
    throw new Error(`Unknown menu name "${currentMenu}"`)
  }

  return <OverlayModal className='bookMenuModal' backdropAction={hideMenu}>
    <div className='bookMenu'>
      {menu}
    </div>
  </OverlayModal>
}


interface BookKeyboardHelpProps {
  closeHelp: () => void
}

function BookKeyboardHelp({closeHelp}: BookKeyboardHelpProps): JSX.Element {
  return <div className='bookMenuInputHelp'>
    <LinkButton label='Go back to main menu' action={closeHelp} />
    <table>
      <tbody>
        <tr>
          <th>Next page</th>
          <td>Right arrow, Page Down</td>
        </tr>
        <tr>
          <th>Previous page</th>
          <td>Left arrow, Page Up</td>
        </tr>
        <tr>
          <th>End of book</th>
          <td>End</td>
        </tr>
        <tr>
          <th>Start of book</th>
          <td>Home</td>
        </tr>
        <tr>
          <th>Open/close menu</th>
          <td>Escape</td>
        </tr>
        <tr>
          <th>Toggle fullscreen</th>
          <td>F</td>
        </tr>
        <tr>
          <th>Close book</th>
          <td>Q</td>
        </tr>
      </tbody>
    </table>
  </div>
}
