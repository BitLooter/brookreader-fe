/** Component displayed at the ends of books */

import React from 'react'
import { Link } from 'react-router-dom'

import { BookViewerContext } from '../../context'
import { BookInfoDetails, UserCommandProperties } from '../../models'
import * as routes from '../../routes'

import { ArrowLeftIcon, BookOpenVariantIcon } from '../icons'
import MediaCoverImg from '../MediaCoverImg'
import OverlayMenu from '../OverlayMenu'
import OverlayModal from '../OverlayModal'

import './BookEnd.css'


interface BookEndProps {
  closeBookEnd: () => void
  highlightedBook?: BookInfoDetails
  loadHighlightedBook?: () => void
  message: string
}

/** Display a menu of options for when the end of a book is reached */
function BookEnd({closeBookEnd, loadHighlightedBook, message, highlightedBook}: BookEndProps): JSX.Element {
  const {closeUrl, collectionKey} = React.useContext(BookViewerContext)

  const menuOptions: UserCommandProperties[] = [
    {label: 'Close book', icon: <ArrowLeftIcon />, route: closeUrl},
    {label: 'Back to reading', icon: <BookOpenVariantIcon />, action: closeBookEnd},
  ]

  let highlightedTitle
  let highlightedCover
  if (highlightedBook) {
    const highlightedUrl = routes.makeBookUrl(collectionKey, highlightedBook.id)
    highlightedTitle = <Link to={highlightedUrl} className='bookEndTitleLink'>
      <div className='bookEndTitle noBackdropAction'>
        {highlightedBook.title}
      </div>
    </Link>
    // Attempting to wrap the cover in a <Link> as above is far more difficult
    // than you would think. Setting the align-self on the Link to stretch and
    // min-height to 0, and using object-fit: contain on the image comes close
    // but breaks backdropAction as the link now covers most of the backdrop.
    // Manually redirecting with onClick works just as well, it just doesn't
    // show the URL in the browser status bar - an acceptable tradeoff IMHO. I
    // may revisit this in the future if I find a way to constrain the link
    // width to the scaled image width in CSS.
    highlightedCover = <MediaCoverImg
      className='bookEndCover noBackdropAction'
      src={highlightedBook.coverUrl}
      catagory='book'
      onClick={loadHighlightedBook}
    />
  }

  return <OverlayModal className='bookEndModal' backdropAction={closeBookEnd}>
    <OverlayMenu items={menuOptions} />
    <div className='bookEndMessage'>{message}</div>
    {highlightedTitle}
    {highlightedCover}
  </OverlayModal>
}

export default BookEnd
