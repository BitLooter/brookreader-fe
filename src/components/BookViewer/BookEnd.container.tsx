/** Component displayed at the ends of books */

import { useContext, useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'

import { BookViewerContext } from '../../context'
import { fetchBookDetails } from '../../fetchers'
import * as routes from '../../routes'
import useKeyboard from '../hooks/usekeyboard'

import BookEnd from './BookEnd'

import type { BookInfoDetails } from '../../models'


enum EndType {
  Next,
  Prev
}

interface BookEndProps {
  closeBookEnd: () => void
  highlightedBookId?: string
}

/** Display a menu of options for when the end of a book is reached
 *
 * @param closeBookEnd  Function called to close the book end
 * @param highlightedBookId  ID of book to display on book end highlight
*/
function BookEndContainer({closeBookEnd, highlightedBookId}: BookEndProps): JSX.Element {
  const {closeBook, collectionKey} = useContext(BookViewerContext)
  const {pageNum: pageNumStr} = useParams<routes.BookRouteParams>()
  const history = useHistory()
  const [highlightedBook, setHighlightedBook] = useState<BookInfoDetails | null>(null)
  useEffect(() => {
    if (highlightedBookId) {
      fetchBookDetails(highlightedBookId).then((details) => setHighlightedBook(details))
    }
  }, [highlightedBookId])
  const pageNum = pageNumStr !== undefined ? Number.parseInt(pageNumStr) : undefined

  let endType: EndType
  if (pageNum === -1) {
    endType = EndType.Prev
  } else {
    endType = EndType.Next
  }

  let message: string
  if (!highlightedBookId) {
    if (endType === EndType.Next) {
      message = 'End of book, use next page key to close book'
    } else {
      message = 'First page of book, use previous page key to close book'
    }
  } else {
    if (!highlightedBook) {
      message = 'Loading book information...'
    } else {
      if (endType === EndType.Next) {
        message = 'End of book, tap cover or use next page key to load next volume'
      } else {
        message = 'First page of book, tap cover or use previous page key to load last volume'
      }
    }
  }

  const loadHighlightedBook = highlightedBook ?
    () => history.push(routes.makeBookUrl(collectionKey, highlightedBook.id)) :
    closeBook

  // Set up keyboard input
  let forwardAction: () => void
  let backAction: () => void
  if (endType === EndType.Next) {
    forwardAction = loadHighlightedBook
    backAction = closeBookEnd
  } else {
    forwardAction = closeBookEnd
    backAction = loadHighlightedBook
  }
  useKeyboard({
    ArrowLeft: backAction,
    ArrowRight: forwardAction,
    Escape: closeBookEnd,
    PageDown: forwardAction,
    PageUp: backAction,
    q: closeBook,
    x: forwardAction,
    z: backAction,
  })

  return <BookEnd
    closeBookEnd={closeBookEnd}
    highlightedBook={highlightedBook ?? undefined}
    loadHighlightedBook={loadHighlightedBook}
    message={message}
  />
}

export default BookEndContainer
