/** Component for downloading and rendering individual pages */

import * as React from 'react'

import usePage from '../hooks/usepage'
import { BookViewerContext, UserSettingsContext } from '../../context'
import './BookPage.css'


/** Render a book page based current page number in BookViewerContext
 *
 * @param pageOffset  Add to pageNum to get working page number. Used e.g. for
 *  prerendering next/previous pages.
 */
function BookPage({pageOffset}: {pageOffset: number}): JSX.Element {
  const {reader, pageDelta, pageNum} = React.useContext(BookViewerContext)
  const {bookAnimatedTransitions} = React.useContext(UserSettingsContext)
  const offsetPageNum = pageNum + pageOffset
  const page = usePage(reader, offsetPageNum)

  // Prevent animations around bookends
  const isBookend = pageNum < 0 || pageNum >= reader.totalPages
  const isLeavingBookend =(pageNum === 0 && pageDelta > 0) ||
    (pageNum === (reader.totalPages - 1) && pageDelta < 0)
  let offsetClassName = bookAnimatedTransitions && !isBookend && !isLeavingBookend ?
    'pageOffsetterAnimated' : ''
  if (pageOffset < 0) {
    offsetClassName += ' pageOffsetPrevious'
  } else if (pageOffset > 0) {
    offsetClassName += ' pageOffsetNext'
  }
  const pageClass = `pageOffsetter ${offsetClassName}`
  const pageStyle = {zIndex: -offsetPageNum}

  return <div className={pageClass} style={pageStyle}>
    {page}
  </div>
}

export default BookPage
