import React from 'react'

import { BookViewerContext } from '../../context'
import useTopBar from '../hooks/usetopbar'

import BookEndContainer from './BookEnd.container'
import BookInputManager from './BookInputManager'
import BookMenu from './BookMenu'
import BookPageManager from './BookPageManager'
import BookProgress from './BookProgress'


/** Renders everything related to the book view */
function BookViewer(): JSX.Element {
  const [menuOpen, setMenuStatus] = React.useState(false)
  const {
    closeUrl, details, pageNum: pageNumParam, reader, setPageNum
  } = React.useContext(BookViewerContext)
  const {setHidden: setTopBarHidden} = useTopBar(closeUrl, details.title, {
    hidden: true,
    overlay: true,
  })
  const pageNum = Number(pageNumParam)

  const showMenu = (): void => setMenuStatus(true)
  const hideMenu = (): void => setMenuStatus(false)

  // Build BookEnd if page number past book boundaries
  let bookend
  let closeBookEnd = (): void => {}
  if (pageNum < 0) {
    // Book start bookend
    closeBookEnd = () => setPageNum(0)
    bookend = <BookEndContainer
      closeBookEnd={closeBookEnd}
      highlightedBookId={details.prevId}
    />
  } else if (pageNum >= reader.totalPages) {
    // Book finished bookend
    closeBookEnd = () => setPageNum(reader.totalPages - 1)
    bookend = <BookEndContainer
      closeBookEnd={closeBookEnd}
      highlightedBookId={details.nextId}
    />
  }

  // Determine what to render depending on if a menu is open
  let overlay
  let progress
  let inputManager
  if (menuOpen) {
    setTopBarHidden(false)
    // Menu renders progress
    overlay = <BookMenu hideMenu={hideMenu} closeRoute={closeUrl} />
    // Menus have own input manager
  } else if (bookend) {
    setTopBarHidden(false)
    // Book end renders progress
    overlay = bookend
    // BookEnds have own input manager
  } else {
    setTopBarHidden(true)
    progress = <BookProgress />
    // Overlay only for menus
    inputManager = <BookInputManager showMenu={showMenu} />
  }

  return <>
    <BookPageManager />
    {inputManager}
    {progress}
    {overlay}
  </>
}

export default BookViewer
