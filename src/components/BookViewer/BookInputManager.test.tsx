import { fireEvent } from '@testing-library/react'
import React from 'react'

import { MOCK_VIEWER_CONTEXT, MOCK_BOOK_PAGENUM } from '../../testdata'
import { renderWithBook } from '../../testutils'
import BookInputManager from './BookInputManager'


test('BookInputManager smoke test', () => {
  renderWithBook(<BookInputManager showMenu={jest.fn()} />)
})

test('BookInputManager keyboard input', () => {
  const setPage = jest.fn()
  const showMenu = jest.fn()

  const {container} = renderWithBook(
    <BookInputManager showMenu={showMenu} />,
    {context: {...MOCK_VIEWER_CONTEXT, setPageNum: setPage}}
  )

  fireEvent.keyDown(container, {key: 'ArrowLeft'})
  expect(setPage).toHaveBeenCalledWith(Number(MOCK_BOOK_PAGENUM) - 1)
  fireEvent.keyDown(container, {key: 'ArrowRight'})
  expect(setPage).toHaveBeenCalledWith(Number(MOCK_BOOK_PAGENUM) + 1)
  fireEvent.keyDown(container, {key: 'Escape'})
  expect(showMenu).toHaveBeenCalled()
})

test.todo('Closing book and other keyboard input')
