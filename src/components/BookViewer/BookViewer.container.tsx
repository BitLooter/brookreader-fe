/** Container component for the book viewer */

import React from 'react'
import { useHistory, useParams } from 'react-router'

import { BookViewerContext, BookViewerContextValue } from '../../context'
import * as routes from '../../routes'
import { setTitle } from '../../util'
import useBook from '../hooks/usebook'
import fetchBookDetails from '../../fetchers/bookdetails'

import LoadingScreen from '../LoadingScreen'
import BookViewer from './BookViewer'

export default BookViewerContainer


interface BookViewerProps {
  bookSource: string | File
}

/** Container for managing the book viewer.
 *
 * @param bookSource  If a string it contains ID of the book to load, if a File it is the book
 */
function BookViewerContainer({bookSource}: BookViewerProps): JSX.Element {
  const book = useBook(bookSource)
  const history = useHistory()
  const params = useParams<routes.BookRouteParams>()
  /** ID of book for which precaching is current */
  const precacheBookId = React.useRef("")
  const pageNum = Number(params.pageNum)
  /** Information about page changes, updated on page change */
  const lastPage = React.useRef({last: pageNum, delta: 0})
  const closeUrl = routes.makeCloseBookUrl(params.collectionKey)

  // Errors on book load are fatal
  if (book.status === 'error') {
    throw book.error
  }

  // Book status updates
  React.useEffect(() => {
    if (book.status === 'ready') {
      // details defined when status is 'ready'
      setTitle(book.details.title)
    }
    return setTitle
  }, [book])

  // Loading screen until book is ready - large books may take some time to load
  if (book.status !== 'ready') {
    return <LoadingScreen
      backRoute={closeUrl}
      title='Loading book'
      message='Loading book...'
    />
  }

  // Type guard book attributes for remaining code, should not be undefined
  if (!book.details || !book.reader) {
    throw new Error('Book details or reader not set: ' + book)
  }

  // Precache info for next/previous books
  if (precacheBookId.current !== book.details.id) {
    // Results can be ignored, simply fetching them will place details in cache
    if (book.details.nextId) {
      fetchBookDetails(book.details.nextId)
    }
    if (book.details.prevId) {
      fetchBookDetails(book.details.prevId)
    }
    precacheBookId.current = book.details.id
  }

  // Information to update on page change
  if (pageNum !== lastPage.current.last) {
    const lastPageKey = 'lastPage_' + book.details.id
    // Record current page accounting for bookends
    if (pageNum < 0) {
      localStorage.setItem(lastPageKey, '0')
    } else if (pageNum >= book.reader.totalPages) {
      // User done reading this book, do not record last page
      localStorage.removeItem(lastPageKey)
    } else {
      localStorage.setItem(lastPageKey, params.pageNum)
    }
    lastPage.current = {last: pageNum, delta: pageNum - lastPage.current.last}
  }

  // Commands used by viewer components
  const setRoutePage = (newPageNum: number): void => {
    history.push(routes.makeBookUrl(params.collectionKey, params.bookId, newPageNum))
  }

  const bookContext: BookViewerContextValue = {
    closeBook: () => history.push(closeUrl),
    closeUrl,
    collectionKey: params.collectionKey,
    details: book.details,
    pageDelta: lastPage.current.delta,
    pageNum,
    reader: book.reader,
    setPageNum: (num) => setRoutePage(num),
  }

  return <>
    <BookViewerContext.Provider value={bookContext}>
      <BookViewer />
    </BookViewerContext.Provider>
  </>
}
