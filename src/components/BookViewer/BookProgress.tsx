/** Component for rendering a view of the progress through a book's pages */

import * as React from 'react'

import { BookViewerContext, UserSettingsContext } from '../../context'
import log from '../../logger'
import { clamp } from '../../util'

import './BookProgress.css'


interface BookProgressProps {
  big?: boolean
  beforeJump?: () => void
}

/** Display progress through the current book. May render nothing if user has
 * hidden progress visibility.
 *
 * @param big: If true will render tall bar with information, else minimal sliver
 * @param beforeJump: Function to call before changing page (e. g. close a menu)
 */
function BookProgress({big, beforeJump}: BookProgressProps): JSX.Element | null {
  const {pageNum, reader, setPageNum} = React.useContext(BookViewerContext)
  const {bookProgressVisible} = React.useContext(UserSettingsContext)

  // Big progress is always rendered, otherwise skip if not visible
  if (!bookProgressVisible && !big) {
    return null
  }

  const jumpPage = (newPageNum: number): void => {
    beforeJump?.()
    setPageNum(newPageNum)
  }
  // Progress ranges from 0.0 (first page) to 1.0 (last page)
  const progress = pageNum / (reader.totalPages - 1)
  const meterClass = `bookProgress ${big ? 'bookProgressBig' : 'bookProgressSmall'}`

  /** Jump to new page when progress bar is clicked. May enhance this UI
   * element later, e.g. with page previews or hover tooltips. */
  const progressClicked = (event: React.MouseEvent<HTMLDivElement>): void => {
    const clickedFraction = event.nativeEvent.offsetX / (event.target as HTMLDivElement).offsetWidth
    const clickedPage = Math.floor(reader.totalPages * clickedFraction)
    log.info(`Progress clicked: ${clickedPage}`)
    jumpPage(clickedPage)
  }

  // Build contents of meter's big mode
  let meterContents
  let clicker
  if (big) {
    const clampedPage = clamp(pageNum, 0, reader.totalPages - 1) + 1
    meterContents = <>
      <span className='progressContentsLeft'>{`Page ${clampedPage} of ${reader.totalPages}`}</span>
    </>
    clicker = <div className='progressClicker' onClick={progressClicked} />
  }

  return (
    <div className={meterClass}>
      <div className='progressBar'>
        <div className='progressBarFill' style={{width: `${progress * 100}%`}} />
      </div>
      <div className='progressContents'>{meterContents}</div>
      {clicker}
    </div>
  )
}

export default BookProgress
