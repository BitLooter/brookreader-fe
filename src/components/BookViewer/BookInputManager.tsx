import React from 'react'

import { BookViewerContext } from '../../context'
import { toggleFullscreen } from '../../util'
import useKeyboard from '../hooks/usekeyboard'

import './BookInputManager.css'


interface BookInputManagerProps {
  showMenu: () => void
}

/** Watches keyboard, mouse, etc. and takes action on input
 *
 * @param showMenu  Function that opens the book viewer menu
 */
function BookInputManager({showMenu}: BookInputManagerProps): JSX.Element {
  const {closeBook, pageNum, reader, setPageNum} = React.useContext(BookViewerContext)
  // Page number is clamped to 1 outside of valid range for bookends
  const goPrevPage = (): void => setPageNum(pageNum > -1 ? pageNum - 1 : -1)
  const goNextPage = (): void => setPageNum(
    pageNum < reader.totalPages ? pageNum + 1 : reader.totalPages)
  const goStart = (): void => setPageNum(0)
  const goEnd = (): void => setPageNum(reader.totalPages - 1)
  useKeyboard({
    ArrowLeft: goPrevPage,
    ArrowRight: goNextPage,
    End: goEnd,
    Escape: showMenu,
    Home: goStart,
    PageDown: goNextPage,
    PageUp: goPrevPage,
    f: toggleFullscreen,
    q: closeBook,
    x: goNextPage,
    z: goPrevPage,
  })

  return <div className='inputOverlayContainer'>
    <div className='inputOverlayInput inputOverlayMenu' onClick={showMenu}>Show menu</div>
    <div className='inputOverlayInput inputOverlayPrev' onClick={goPrevPage}>Previous page</div>
    <div className='inputOverlayInput inputOverlayNext' onClick={goNextPage}>Next page</div>
  </div>
}

export default BookInputManager
