import React from 'react'

import { BookViewerContext } from '../../context'
import BookPage from './BookPage'
import './BookPageManager.css'

export default BookPageManager


const NEXT_OFFSET = 1
const PREV_OFFSET = -1

/** Renders everything related to the book view */
function BookPageManager(): JSX.Element {
  const {pageNum, reader} = React.useContext(BookViewerContext)

  return <div className='pageContainer'>
    {pageNum > 0 &&
      <BookPage key={pageNum + PREV_OFFSET} pageOffset={PREV_OFFSET} />}
    <BookPage key={pageNum} pageOffset={0} />
    {pageNum < reader.totalPages - 1 &&
      <BookPage key={pageNum + NEXT_OFFSET} pageOffset={NEXT_OFFSET} />}
  </div>
}
