import { renderHook } from '@testing-library/react-hooks'
import React from 'react'

import { TopBarContext } from '../../context'
import useTopBar from './usetopbar'


const TEST_BACK_URL = 'BACK_URL'
const TEST_TITLE = 'TITLE'

test('Setting TopBar properties', () => {
  const setHidden = jest.fn()
  const setNav = jest.fn()
  const setOverlay = jest.fn()
  const setupFn = jest.fn()

  const {result} = renderHook( () => useTopBar(TEST_BACK_URL, TEST_TITLE), {
    wrapper: ({children}) => (
      <TopBarContext.Provider
        value={{
          noProvider: false,
          setHidden,
          setNav,
          setOverlay,
          setup: setupFn,
        }}
      >
        {children}
      </TopBarContext.Provider>
    )
  })

  expect(setupFn.mock.calls[0][0].backUrl).toEqual(TEST_BACK_URL)
  expect(setupFn.mock.calls[0][0].title).toEqual(TEST_TITLE)
  result.current.setHidden(true)
  expect(setHidden).toBeCalledWith(true)
  result.current.setNav('newback', 'newtitle')
  expect(setNav).toBeCalledWith('newback', 'newtitle')
  result.current.setOverlay(true)
  expect(setOverlay).toBeCalledWith(true)
})

test('useTopBar errors with no context', () => {
  const {result} = renderHook( () => useTopBar(TEST_BACK_URL, TEST_TITLE) )
  expect(result.error.message).toEqual('No provider for TopBar set')
})
