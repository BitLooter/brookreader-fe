import { renderHook } from '@testing-library/react-hooks'

import { MOCK_CBZ_DATA } from '../../testdata'
import useBook from './usebook'


test('Load local book', async () => {
  // useBook may create object URLs that need to be cleaned up
  window.URL.revokeObjectURL = jest.fn()
  const {result, waitForNextUpdate} = renderHook( () => useBook(MOCK_CBZ_DATA) )
  await waitForNextUpdate()
  const bookInfo = result.current
  expect(bookInfo.details.id).toBe('local')
})
