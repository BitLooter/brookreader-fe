import { useEffect, useState } from 'react'

import { fetchCatalogIndex } from '../../fetchers'
import { CatalogIndex, DownloadStatus } from '../../models'


export type CatalogStatus = DownloadStatus<CatalogIndex>
const initialStatus: CatalogStatus = {
  status: 'uninitialized'
}

/** React hook to load a book index from a given bookshelf URL */
function useCatalogIndex(catalogIndexUrl: string): CatalogStatus {
  const [catalogIndex, setCatalogIndex] = useState<CatalogStatus>(initialStatus)
  useEffect(() => {
    fetchCatalogIndex(catalogIndexUrl)
      .then( (index: CatalogIndex) => setCatalogIndex({
        status: 'ready',
        response: index
      }) )
      .catch( (reason) => setCatalogIndex({
        status: 'error',
        error: reason
      }) )
  }, [catalogIndexUrl])

  return catalogIndex
}

export default useCatalogIndex
