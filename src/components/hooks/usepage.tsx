/** Hook for rendering a page from a reader */

import React from 'react'

import { BookContentType, ReaderBase } from '../../bookreader'
import { clamp } from '../../util'

import BookPageError from '../BookViewer/BookPageError'
import LoadingMessage from '../LoadingMessage'


/** Creates a rendered page based on given reader and pageNum. If an error
 * occurs while loading renders an error message instead. */
export default function usePage(reader: ReaderBase, pageNum: number): JSX.Element {
  const [page, setPage] = React.useState<JSX.Element>(
    <LoadingMessage message={`Loading page ${pageNum + 1}`} />
  )
    // Errors on page load are not fatal, show an error message in place of page
    const handleError = React.useCallback(
    () => setPage(buildError('Could not load page image', pageNum)),
    [pageNum]
  )
  React.useEffect( () => {
    buildPageOrError(reader, clamp(pageNum, 0, reader.totalPages - 1), handleError)
      .then( (renderedPage) => setPage(renderedPage))
  }, [handleError, pageNum, reader])

  return page
}

/** Builds a page and substitutes an error page if needed */
async function buildPageOrError(reader: ReaderBase, pageNum: number, onError: () => void): Promise<JSX.Element> {
  let element: JSX.Element
  try {
    element = await buildPage(reader, pageNum, onError)
  } catch (e) {
    element = buildError(e.message, pageNum)
  }
  return element
}

/** Converts raw book data into a React component. onError passed to img tag
 * as its onerror handler. */
async function buildPage(reader: ReaderBase, pageNum: number, onError: () => void): Promise<JSX.Element> {
  if (reader.bookType !== BookContentType.Image) {
    throw new TypeError(`Unknown bookType ${reader.bookType}`)
  }

  const newImageUrl = await reader.getPageUrl(pageNum)
  const altText = `Page ${pageNum}`
  const element = <img src={newImageUrl} alt={altText} className='pageImage' onError={onError} />
  return element
}

function buildError(message: string, pageNum: number): JSX.Element {
  return <BookPageError message={message} pageNum={pageNum} />
}
