/** Hook and related code for loading books */

import React from 'react'

import loadBookReader, { ReaderBase } from '../../bookreader'
import { fetchBookDetails } from '../../fetchers'
import { makeLogger } from '../../logger'
import { BookInfoDetails } from '../../models'
import { fingerprintFile, VOID_FUNC } from '../../util'

import DEFAULT_COVER from '../../images/default_cover.png'


export default useBook

const log = makeLogger('useBook')

/** Initial state before anything has been done */
interface BookStateUninitialized {
  status: 'uninitialized'
  /** Unique identifier for remote and local books */
  fingerprint: string
}
/** Loading has started but nothing is ready */
interface BookStateStarted {
  status: 'started'
  /** Unique identifier for remote and local books */
  fingerprint: string
}
/** Book details have been downloaded and are ready */
interface BookStateDetails extends Omit<BookStateStarted, 'status'> {
  status: 'details'
  /** Full book details, set when book details JSON downloaded */
  details: BookInfoDetails
}
/** Reader is ready and loading complete */
interface BookStateReady extends Omit<BookStateDetails, 'status'> {
  status: 'ready'
  /** Book reader object set when finished loading */
  reader: ReaderBase
}
/** An error was encountered while loading */
interface BookStateError extends Omit<BookStateStarted, 'status'> {
  status: 'error'
  /** Error encountered loading book */
  error: Error
}
export type BookState = BookStateUninitialized | BookStateStarted |
  BookStateDetails | BookStateReady | BookStateError

type CleanupPromise = Promise<() => void>

const UNINITIALIZED_BOOK: BookStateUninitialized = {
  status: 'uninitialized',
  fingerprint: ''
}

/** React hook to manage book state. Give it an ID or File and it will handle
 * loading the book data. Will trigger a state change when book finishes
 * loading, book remains null until then.
 *
 * useBook should not throw any errors, but rather errors will be reported by
 * setting status to 'error' and book.error to the error object.
 *
 * @param bookSource: ID of book or File blob containing book
 */
function useBook(bookSource: string | File): BookState {
  const [book, dispatchBook] = React.useReducer(bookReducer, UNINITIALIZED_BOOK)

  React.useEffect( () => {
    let cleanupPromise: CleanupPromise
    if (bookSource instanceof File) {
      // Details from File blob
      log.debug(`Loading book from local file ${bookSource.name}`)
      cleanupPromise = loadBookFromFile(bookSource, dispatchBook)
    } else {
      // Details from loading metadata file
      log.debug(`Loading book from ID ${bookSource}`)
      cleanupPromise = loadBookFromId(bookSource, dispatchBook)
    }

    return () => {
      cleanupPromise.then((bookCleanup) => bookCleanup())
    }
  }, [bookSource])

  return book
}

/** Fetches a book and related metadata from an ID */
async function loadBookFromId(id: string, dispatch: React.Dispatch<BookReducerAction>): CleanupPromise {
  // Loading started
  dispatch({type: 'init', payload: id})

  // Get book details
  let details: BookInfoDetails
  try {
    log.debug(`Retrieving book details for ${id}`)
    details = await fetchBookDetails(id)
  } catch (e) {
    dispatch({type: 'error', payload: new Error(
      `Can't get book details for "${id}": ${e.message}`
    )})
    return VOID_FUNC
  }
  // Loading book details done
  dispatch({type: 'details', payload: details})

  // Create book reader
  let reader: ReaderBase
  log.debug(`Retrieving book for ${id}`)
  const bookFormat = details.pageUrls ? 'dir' : undefined
  try {
    reader = await loadBookReader(
      details.url,
      {type: bookFormat, extra: details.pageUrls}
    )
  } catch (e) {
    dispatch({type: 'error', payload: new Error(
      `Can't load book from "${details.url}": ${e.message}`
    )})
    return VOID_FUNC
  }

  // Loading complete, finalize data
  dispatch({type: 'done', payload: reader})

  return () => reader.cleanup()
}

/** Creates a book from a File blob */
async function loadBookFromFile(file: File, dispatch: React.Dispatch<BookReducerAction>): CleanupPromise {
  dispatch({type: 'init', payload: fingerprintFile(file)})
  const details: BookInfoDetails = {
    id: 'local',
    title: file.name,
    dateAdded: new Date(),
    url: file.name,
    // Cover URLs are generated later from reader
    coverThumbUrl: '',
    coverUrl: '',
  }
  dispatch({type: 'details', payload: details})

  let reader
  try {
    reader = await loadBookReader(file, {filename: file.name})
  } catch (e) {
    dispatch({type: 'error', payload: new Error(
      `Cannot load local book: ${e.message}`
    )})
    return VOID_FUNC
  }

  // Extract first page for cover
  let coverUrl: string
  try {
    coverUrl = await reader.getPageUrl(0)
  } catch (e) {
    log.error(`Could not extract cover from Blob: ${e.message}`)
    coverUrl = DEFAULT_COVER
  }

  dispatch({
    type: 'done',
    payload: reader
  })

  return () => window.URL.revokeObjectURL(coverUrl)
}

// Book state reducer
//////////////////////

interface BookReducerActionInit {
  type: 'init'
  payload: string
}
interface BookReducerActionDetails {
  type: 'details'
  payload: BookInfoDetails
}
interface BookReducerActionDone {
  type: 'done'
  payload: ReaderBase
}
interface BookReducerActionError {
  type: 'error'
  payload: Error
}
type BookReducerAction = BookReducerActionInit | BookReducerActionDetails |
  BookReducerActionDone | BookReducerActionError

  /** Reducer for managing useBook's useReducer state */
function bookReducer(state: BookState, action: BookReducerAction): BookState {
  switch (action.type) {
    // Payload is new book fingerprint
    case 'init': return {
      fingerprint: action.payload,
      status: 'started'
    }
    // Payload is book details
    case 'details': return {...state, details: action.payload, status: 'details'}
    // Payload is book reader, previous state contains details
    case 'done': return {...state as BookStateDetails, reader: action.payload, status: 'ready'}
    // Payload is an Error object
    case 'error': return {...state, error: action.payload, status: 'error'}
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    default: throw new Error(`useBook: Unknown reducer action '${(action as any).type}'`)
  }
}
