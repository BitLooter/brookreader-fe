import { CatalogStats, DownloadStatus } from '../../../models'
import { TEST_STATS } from '../../../testdata'


function useStats(): DownloadStatus<CatalogStats> {
  return {
    response: TEST_STATS
  }
}

export default useStats
