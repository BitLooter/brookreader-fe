import { TEST_BOOK_DETAILS, TEST_READER_10 } from '../../../testdata'
import { BookState } from '../usebook'


function useBook(): BookState {
  return {
    status: 'ready',
    details: TEST_BOOK_DETAILS,
    fingerprint: 'TEST_FINGERPRINT',
    reader: TEST_READER_10,
  }
}

export default useBook
