import { TEST_INDEX } from '../../../testdata'

import type { CatalogIndex, DownloadStatus } from '../../../models'


function useBookIndex(bookIndexUrl: string): DownloadStatus<CatalogIndex> {
  return {status: 'ready', response: TEST_INDEX}
}

export default useBookIndex
