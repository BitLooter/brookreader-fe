import React from 'react'

import { fetchStats } from '../../fetchers'
import { CatalogStats, DownloadStatus } from '../../models'


type StatsStatus = DownloadStatus<CatalogStats>
const initialStatus: StatsStatus = {
  status: 'uninitialized'
}
/** Returns catalog stats and handles errors if present */
function useStats(): StatsStatus {
  const [stats, setStats] = React.useState<StatsStatus>(initialStatus)
  React.useEffect( () => {
    fetchStats()
      .then( (newStats) => setStats({
        status: 'ready',
        response: newStats,
      }) )
      .catch ( (reason) => setStats({
        status: 'error',
        error: reason
      }) )
  }, [])
  return stats
}

export default useStats
