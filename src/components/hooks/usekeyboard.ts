import React from 'react'

import registerKeyBindings from '../../keyboard'


export interface KeyBindings {[key: string]: () => void}

/** Binds mapping of key commands to functions. Automatically wraps with
 * useEffect and handles cleanup.
 */
function useKeyboard(bindings: KeyBindings): void {
  React.useEffect( () => {
    registerKeyBindings(bindings)
    return () => registerKeyBindings({})
  }, [bindings])
}

export default useKeyboard
