import React from 'react'

import { enterFullscreen, exitFullscreen, isFullscreen } from '../../util'


/** Manages document fullscreen state. Returns tuple of values, first is a
 * boolean representing the current state, second is a function that takes a
 * boolean value to set the current state.
 */
function useFullscreen(): [boolean, (fullscreen: boolean) => void] {
  const [screenIsFull, setScreenIsFull] = React.useState(isFullscreen())
  React.useEffect(() => {
    const fullscreenHandler = (event: Event): void => {
      if (isFullscreen()) {
        setScreenIsFull(true)
      } else {
        setScreenIsFull(false)
      }
    }
    document.addEventListener('fullscreenchange', fullscreenHandler)
    return () => document.removeEventListener('fullscreenchange', fullscreenHandler)
  }, [])

  const setFullscreenState = (fullscreen: boolean): void => {
    if (fullscreen) {
      enterFullscreen()
    } else {
      exitFullscreen()
    }
  }
  return [screenIsFull, setFullscreenState]
}

export default useFullscreen
