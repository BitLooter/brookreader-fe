import { renderHook } from '@testing-library/react-hooks'

import { dummyReader } from '../../bookreader/dummyreader'
import usePage from './usepage'


test.todo('Better tests')
test('Load book page', async () => {
  const {result, waitForNextUpdate} = renderHook( () => usePage(dummyReader, 0) )
  await waitForNextUpdate()
  expect(result.current).not.toBeNull()
})
