import { renderHook } from '@testing-library/react-hooks'

import useStats from './usestats'


jest.mock('../../fetchers')

test('useStats fetches correct data', async () => {
  const {result, waitForNextUpdate} = renderHook( () => useStats() )
  // Result is null before data finishes downloading
  expect(result.current.response).toBeUndefined()
  expect(result.current.error).toBeUndefined()
  await waitForNextUpdate()
  expect(result.current.response).not.toBeUndefined()
  expect(result.current.response.server.version).toBe('0.0.0')
  expect(result.current.response.server.date).toBeInstanceOf(Date)
})

test.todo('Error status')
