import React from 'react'

import { TopBarIcon } from '../TopBar'
import { TopBarContext } from '../../context'


interface TopBarControls {
  setHidden: (hidden: boolean) => void
  setNav: (backUrl: string, title: string) => void
  setOverlay: (overlay: boolean) => void
}

interface TopBarHookOptions {
  icons?: TopBarIcon[]
  hidden?: boolean
  overlay?: boolean
}

/** Settings used if no options object given */
const topBarDefaults: TopBarHookOptions = {
  icons: [],
  hidden: false,
  overlay: false
}

/** Set initial options for the top bar and provide controls to change it.
 * Obtains optional options object orchestrating other options:
 *    hidden: Set true to make bar invisible
 *    overlay: If true bar is removed from flow and placed absolutely at top
 */
function useTopBar(
  backUrl: string | null,
  title: string,
  {icons, hidden, overlay} = topBarDefaults
): TopBarControls
{
  const {noProvider, setHidden, setOverlay, setNav, setup} = React.useContext(TopBarContext)

  if (noProvider) {
    throw new Error('No provider for TopBar set')
  }

  // Set initial bar state
  React.useEffect( () => {
    setup({
      backUrl,
      title,
      overlay: overlay ? overlay : false,
      hidden: hidden ? hidden : false,
      icons: icons ? icons : []
    }) },
    // Causes infinite recursion errors if this rule is followed. May look at
    // this again later if TopBar configuration/rendering changes.
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  return {setHidden, setNav, setOverlay}
}

export default useTopBar
