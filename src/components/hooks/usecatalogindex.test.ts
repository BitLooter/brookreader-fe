import { renderHook } from '@testing-library/react-hooks'

import useCatalogIndex from './usecatalogindex'


jest.mock('../../fetchers/catalogindex')

test('Load catalog index', async () => {
  const {result, waitForNextUpdate} = renderHook( () => useCatalogIndex('Book index URL') )
  await waitForNextUpdate()
  expect(result.current).not.toBeNull()
})
