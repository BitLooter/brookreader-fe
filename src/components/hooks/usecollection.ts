/** Hook for getting a specific shelf based on a collection key */

import useCatalogIndex from './usecatalogindex'
import config from '../../config'
import { makeLogger } from '../../logger'
import { CatalogCollection, DownloadStatus } from '../../models'


const log = makeLogger('CatalogContainer')

export type CollectionStatus = DownloadStatus<CatalogCollection | undefined>

/** Mapping of bookshelf names to URLs */
const BOOKSHELF_URLS: {[indexName: string]: string} = {
  bookshelf: `${config.dataBaseUrl}/bookshelf.json`,
  showcase: `${config.dataBaseUrl}/showcase.json`,
}
/** Url of index to use when no collectionBookshelf specified */
const BOOKSHELF_DEFAULT_URL = BOOKSHELF_URLS.bookshelf


/**
*/
export default function useCollection(collectionKey: string): [CollectionStatus, string] {
  const [collectionId, collectionBookshelf] = collectionKey.split('@')
  const bookshelfUrl = BOOKSHELF_URLS[collectionBookshelf] ?? BOOKSHELF_DEFAULT_URL
  const catalogStatus = useCatalogIndex(bookshelfUrl)

  log.debug('Catalog collection ' + collectionKey)

  switch (catalogStatus.status) {
    case 'error':
      return [{status: 'error', error: catalogStatus.error}, collectionId]
    case 'loading':
    case 'uninitialized':
      return [{status: catalogStatus.status}, collectionId]
    case 'ready': {
      const collection = collectionId ?
        catalogStatus.response.collections[collectionId] :
        catalogStatus.response.rootCollection
      return [
        {status: 'ready', response: collection},
        collectionId
      ]
    }
  }
  // if (catalogStatus.status === 'error') {
  //   return [{status: 'error', error: catalogStatus.error}, collectionId]
  // } else if (catalogStatus.status === 'loading' || 'uninitialized') {
  //   return [{status: 'loading'}, collectionId]
  // } else {
  //   const collection = collectionId ?
  //     catalogStatus.response.collections[collectionId] :
  //     catalogStatus.response.rootCollection
  //   return [
  //     {status: 'ready', response: collection},
  //     collectionId
  //   ]
  // }
}
