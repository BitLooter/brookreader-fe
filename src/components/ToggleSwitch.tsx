/** Component for a switch that can be toggled on or off */

import * as React from 'react'

import './ToggleSwitch.css'


interface ToggleSwitchProps {
  initialState: boolean
  label?: string
  onToggle: (newState: boolean) => void
}

/** Renders a switch that can be toggled on or off
 *
 * @param initialState  Start enabled or disabled, ignored after first render
 * @param onToggle  Function called with the new state whenever it changes
 */
function ToggleSwitch({initialState, label, onToggle}: ToggleSwitchProps): JSX.Element {
  const [enabled, setEnabled] = React.useState(initialState)
  const toggleClicked = (): void => {
    setEnabled(!enabled)
    onToggle(!enabled)
  }
  const toggleState = enabled ? 'toggleEnabled' : 'toggleDisabled'

  return <div className='noBackdropAction toggleContainer' onClick={toggleClicked}>
    <div className='toggleLabel'>{label}</div>
    <div className={'toggleSliderContainer ' + toggleState}>
      <div className='toggleText toggleOff'>OFF</div>
      <div className='toggleText toggleOn'>ON</div>
      <div className='toggleSlider'></div>
    </div>
  </div>
}

export default ToggleSwitch
