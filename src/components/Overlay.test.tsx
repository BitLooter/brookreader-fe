import { render } from '@testing-library/react'
import React from 'react'

import { renderWithRouter } from '../testutils'

import OverlayModal from './OverlayModal'
import OverlayMenu from './OverlayMenu'
import OverlayMenuItem from './OverlayMenuItem'


test('OverlayModal renders', () => {
  render(<OverlayModal children={null} />)
})

test('OverlayMenu renders', () => {
  const backdropCallback = jest.fn()
  renderWithRouter(<OverlayMenu backdropAction={backdropCallback} items={[]} />)
})

test('OverlayMenuItem renders', () => {
  renderWithRouter(<OverlayMenuItem label='Test' />)
})
