import React from 'react'
import { Link } from 'react-router-dom'

import useFullscreen from './hooks/usefullscreen'

import { ArrowLeftIcon, FullscreenExitIcon, FullscreenIcon, HomeIcon } from './icons'
import './TopBar.css'


export interface TopBarIcon {
  icon: JSX.Element
  label: string
  action: () => void
}

interface TopBarProps {
  backUrl: string | null
  icons?: TopBarIcon[]
  overlay?: boolean
  title: string
  hidden?: boolean
}

/** Navigation/info/tool bar at top of screen.
 *   backUrl: Route to link to when back arrow is clicked. If null, link does
 *     nothing and icon is changed to home
 *   title: Text displayed in bar
 */
function TopBar({backUrl, icons, overlay, title, hidden}: TopBarProps): JSX.Element {
  const [screenIsFull, setFullscreen] = useFullscreen()

  let className = 'barNav'
  if (overlay) {
    className += ' barNavOverlay'
  }
  if (hidden) {
    className += ' barNavHidden'
  }

  const backLink = backUrl === null ?
    <HomeIcon /> : <Link to={backUrl}><ArrowLeftIcon /></Link>

  const extraIcons = []
  for (const iconParams of icons ? icons : []) {
    extraIcons.push(
      <div onClick={iconParams.action} key={iconParams.label}>{iconParams.icon}</div>
    )
  }

  const toggleFullscreenState = (): void => {
    setFullscreen(!screenIsFull)
  }
  const fullscreenIcon = screenIsFull ?
    <FullscreenExitIcon onClick={toggleFullscreenState} /> :
    <FullscreenIcon onClick={toggleFullscreenState} />

  return <>
    <nav className={className}>
      {backLink}
      <span className='barTitle'>{title}</span>
      {extraIcons}
      {fullscreenIcon}
    </nav>
  </>
}

export default TopBar
