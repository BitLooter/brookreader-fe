/** Main container component for the whole app */

import React from 'react'
import {
  HashRouter, Redirect, Route, Switch,
  useParams, useHistory
} from 'react-router-dom'

import {
  DEFAULT_ROOT_SHELF, SETTINGS_LOCALSTORAGE_KEY, UserSettings,
  loadUserSettings, saveUserSettings
} from '../config'
import { UserSettingsContext, UserSettingsContextValue } from '../context'
import log from '../logger'
import * as routes from '../routes'
import sendToast from '../toaster'

import About from './About'
import ErrorBoundary from './ErrorBoundary'
import LoadingScreen from './LoadingScreen'
import MainApp from './MainApp'
/** Lazy-loaded BookViewerContainer */
const BookViewerContainer = React.lazy( () => import('./BookViewer/BookViewer.container') )
/** Lazy-loaded CatalogContainer */
const CatalogContainer = React.lazy( () => import('./Catalog/Catalog.container') )
/** Lazy-loaded HomeContainer */
const HomeContainer = React.lazy( () => import('./Home/Home.container') )

export default MainAppContainer


type BookRouteNoPageParams = Omit<routes.BookRouteParams, "pageNum">

/** Help information on Catalog error boundary */
const CATALOG_ERROR_INFO = `Unfortunately the catalog cannot be read. This
  could be a temporary issue, refresh the page or try again later.`
/** Help information on BookViewer error boundary */
const BOOK_ERROR_INFO = `Unfortunately this book cannot be read. This could be
  a temporary issue, refresh the page or try again later.`
/** Suspense fallback for catalog viewer */
const CATALOG_SUSPENSE = <LoadingScreen
  backRoute={routes.homeUrl}
  title='Loading catalog'
  message='Loading catalog browser...'
/>
/** Suspense fallback for home page */
const HOME_SUSPENSE = <LoadingScreen
  backRoute={routes.homeUrl}
  title='Loading home'
  message='Loading home...'
/>

// Components
///////////////

/** Entry point for the app, coordinates between the different sections */
function MainAppContainer(): JSX.Element {
  const localFile = React.useRef<File | undefined>()
  const settingsContext = useSettingsContext()

  const routeSwitch = <Switch>
    <Route path={routes.aboutUrl}>
      <About />
    </Route>
    <Route path={routes.bookPattern}>
      <BookRoute localFileRef={localFile} />
    </Route>
    <Route path={routes.bookNoPagePattern}> <BookRouteNoPage /> </Route>
    <Route path={routes.catalogPattern}>
      <CatalogRoute />
    </Route>
    <Route path={routes.showcasePattern}>
      <CatalogRoute isShowcase={true} />
    </Route>
    <Route path={routes.homeUrl} exact={true}>
      <HomeRoute localFileRef={localFile} />
    </Route>
    <Redirect to={routes.homeUrl} />
  </Switch>

  return (
    <HashRouter>
      <UserSettingsContext.Provider value={settingsContext}>
        <MainApp>
          {routeSwitch}
        </MainApp>
      </UserSettingsContext.Provider>
    </HashRouter>
  )
}

/** Helper hook to set up a stateful value for UserSettingsContext */
function useSettingsContext(): UserSettingsContextValue {
  const [userSettings, setUserSettings] = React.useState(loadUserSettings)

  // Update the settings state if another tab changes a setting
  React.useEffect( () => {
    const updateHandler = (ev: StorageEvent): void => {
      if (ev.key === SETTINGS_LOCALSTORAGE_KEY && ev.newValue) {
        log.info('Receiving new user settings', 'config')
        setUserSettings(JSON.parse(ev.newValue))
      }
    }
    window.addEventListener('storage', updateHandler)
    return () => window.removeEventListener('storage', updateHandler)
  }, [])

  return {
    ...userSettings,
    updateSetting: (name, value) => {
      setUserSettings((oldSettings: UserSettings) => {
        const newSettings = {...oldSettings}
        newSettings[name] = value
        saveUserSettings(newSettings)
        return newSettings
      })
    }
  }
}

// Route components
////////////////////

interface HomeRouteProps {
  localFileRef: React.MutableRefObject<File | undefined>
}

/** Router component for home page
 *
 * @param localFileRef  React ref containing File blob to load in BookRoute
*/
function HomeRoute({localFileRef}: HomeRouteProps): JSX.Element {
  const history = useHistory()
  /** Loads a book from a local File object */
  function loadLocalBook(bookFile: File): void {
    localFileRef.current = bookFile
    // Redirect to book route
    history.push(routes.makeBookUrl('local', 'local'))
  }

  return <React.Suspense fallback={HOME_SUSPENSE}>
    <HomeContainer loadLocalFile={loadLocalBook} />
  </React.Suspense>
}

interface BookRouteProps {
  /** React-router does not rerender on route changes so we need the ref to get
   * the current value of localFile. */
  localFileRef: React.RefObject<File | undefined>
}

/** Router component for book viewer
 *
 * @param localFileRef  React ref containing File blob used if bookId is 'local'.
*/
function BookRoute({localFileRef}: BookRouteProps): JSX.Element {
  const {bookId, collectionKey} = useParams<routes.BookRouteParams>()
  const backRoute = routes.makeCatalogUrl(collectionKey)

  // Go back to home if local book from a previous (unavailable) session
  if (bookId === 'local' && !localFileRef.current) {
    // Local book only valid until session ends
    log.debug('Local book without data, redirecting to Home', 'Router')
    sendToast('Invalid link, local books must be reopened')
    return <Redirect to={routes.homeUrl} />
  }

  const source = bookId === 'local' ?
    localFileRef.current as File :  // Known not to be null from check above
    bookId

  const suspenseFallback = <LoadingScreen
    backRoute={backRoute}
    title='Loading book'
    message='Loading book viewer...'
  />

  return <React.Suspense fallback={suspenseFallback}>
    <ErrorBoundary
      backUrl={backRoute}
      info={BOOK_ERROR_INFO}
      title='An error occured reading this book'
      titleShort='Book error'
    >
      <BookViewerContainer bookSource={source} />
    </ErrorBoundary>
  </React.Suspense>
}

/** Router component that redirects to the normal book route. Uses last viewed
 * page if available, otherwise defaults to 0.
 */
function BookRouteNoPage(): JSX.Element {
  const {bookId, collectionKey} = useParams<BookRouteNoPageParams>()
  // If not in localStorage page number will be 0
  const initialPage = Number(localStorage.getItem('lastPage_' + bookId))
  log.info('Page number not specified, redirecting to default', 'Router')
  return <Redirect to={routes.makeBookUrl(collectionKey, bookId, initialPage)} />
}

interface CatalogRouteProps {
  isShowcase?: boolean
}

/** Router component for catalog/showcase views
 *
 * @param isShowcase  Set true if this is a showcase catalog
*/
function CatalogRoute({isShowcase}: CatalogRouteProps): JSX.Element {
  const params = useParams<routes.CatalogRouteParams>()
  const collectionParam = params.collectionKey ?? DEFAULT_ROOT_SHELF
  // The /showcase/ route has an implicit @showcase bookshelf name
  const collectionKey = isShowcase ? collectionParam + '@showcase' : collectionParam
  return <React.Suspense fallback={CATALOG_SUSPENSE}>
    <ErrorBoundary
      backUrl={routes.homeUrl}
      info={CATALOG_ERROR_INFO}
      title='An error occured loading the catalog'
      titleShort='Catalog error'
    >
      <CatalogContainer
        collectionKey={collectionKey}
        isShowcase={isShowcase}
      />
    </ErrorBoundary>
  </React.Suspense>
}
