import React from 'react'

import config from '../config'
import * as routes from '../routes'
import { setTitle } from '../util'
import useStats from './hooks/usestats'
import useTopBar from './hooks/usetopbar'

import LinkButton from './LinkButton'
import './About.css'


function About(): JSX.Element {
  useTopBar(routes.homeUrl, 'About ' + config.siteName)
  const stats = useStats()
  React.useEffect( () => {
    setTitle('About')
    return setTitle
  }, [])

  let scanDate
  let serverVersion
  if (stats.status === 'error') {
    scanDate = <span className='aboutError'>{stats.error.name}: {stats.error.message}</span>
    serverVersion = <span className='aboutError'>{stats.error.name}: {stats.error.message}</span>
  } else if (stats.status === 'uninitialized' || stats.status === 'loading') {
    scanDate = <span className='aboutLoading'>Loading...</span>
    serverVersion = <span className='aboutLoading'>Loading...</span>
  } else {
    scanDate = `${stats.response.server.date.toDateString()}
                ${stats.response.server.date.toLocaleTimeString()}`
    serverVersion = 'v' + stats.response.server.version
  }

  return <div className='aboutContainer'>
    <div className='aboutContent'>
      <p className='aboutDescription'>{config.aboutMessage}</p>
      <p className='aboutScanDate'>Book database generated {scanDate}</p>
      <h2>Software versions</h2>
      <p>
        Brookreader is copyright &copy; 2019 David Powell under AGPLv3.0.
      </p>
      <p>
        <a href='https://gitlab.com/BitLooter/brookreader-fe'>
          Brookreader Frontend
        </a> v{config.version}<br />
        <a href='https://gitlab.com/BitLooter/brookreader-admin'>
          Brookreader Admin
        </a> {serverVersion}<br/>
      </p>
      <h2>Copyrights</h2>
      <p>
        Brookreader incorporates code from several open-source projects,
        including the following:
      </p>
      <ul>
        <li>
          <a href='https://nodeca.github.io/pako/'>pako</a> &copy; 2014-2017
          <a href='https://github.com/nodeca/pako/blob/master/LICENSE'> MIT</a> by Vitaly Puzrin and Andrei Tuputcyn
        </li>
        <li>
          <a href='https://reactjs.org/'>React</a> &copy;
          <a href='https://github.com/facebook/react/blob/master/LICENSE'> MIT</a> by Facebook, Inc. and its affiliates
        </li>
        <li>
          <a href='https://github.com/thebuilder/react-intersection-observer'>react-intersection-observer</a> &copy; 2017
          <a href='https://github.com/thebuilder/react-intersection-observer/blob/master/LICENSE'> MIT</a> by React Intersection Observer authors
        </li>
        <li>
          <a href='https://reacttraining.com/react-router/'>React Router</a> &copy; 2016-2018
          <a href='https://github.com/ReactTraining/react-router/blob/master/LICENSE'> MIT</a> by React Training
        </li>
        <li>
          <a href='https://materialdesignicons.com/'>Various icons</a> &copy;
          <a href='https://dev.materialdesignicons.com/license'> MIT</a> by Google, Austin Andrews, or Michael Irigoyen
        </li>
      </ul>
    </div>

    <div className='aboutButtons'>
      <LinkButton label='Home' route={routes.homeUrl} />
    </div>
  </div>
}

export default About
