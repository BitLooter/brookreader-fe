import React from 'react'

import './ErrorBox.css'


export default ErrorBox

interface ErrorBoxProps {
  title: string
  message: string
}

/** Simple non-fatal error display
 *
 * @param title: Text to display as error titlex
 * @param message: Text to display as error bodyx
*/
function ErrorBox({title, message}: ErrorBoxProps): JSX.Element {
  return <div className='errorBoxContainer'>
    <h1 className='errorBoxTitle'>{title}</h1>
    <p className='errorBoxMessage'>{message}</p>
  </div>
}
