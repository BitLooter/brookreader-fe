import React from 'react'
import { Link } from 'react-router-dom'

import { UserCommandProperties } from '../models'

import './LinkButton.css'


export default LinkButton

const LINK_BUTTON_CLASS = 'linkButton noBackdropAction'

/** Display a clickable button. Calls action if specified, otherwise follows
 * route/URL. See UserCommandProperties for valid parameters. */
function LinkButton({action, label, route}: UserCommandProperties): JSX.Element {
  if (action) {
    return <button className={LINK_BUTTON_CLASS} onClick={action}>{label}</button>
  } else if (route) {
    return <Link to={route}>
      <button className={LINK_BUTTON_CLASS}>{label}</button>
    </Link>
  } else {
    throw new Error('LinkButton: Must specify action or URL')
  }
}
