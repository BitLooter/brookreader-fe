import React from 'react'

import { UserCommandProperties } from '../models'

import './OverlayMenu.css'
import OverlayMenuItem from './OverlayMenuItem'


interface OverlayMenuProps {
  items: UserCommandProperties[]
}

/** Display menu items as text links with icons. Designed to be displayed over
 * other content as an overlay. If anywhere besides a menu item is clicked,
 * call backdropAction (e.g. to close the menu). You can give child elements
 * the noBackdropAction CSS class and they will not call backdropAction when
 * clicked.
 *
 * Renders a div with the class "overlayMenu".
 */
function OverlayMenu({items}: OverlayMenuProps): JSX.Element {
  const menuItems: JSX.Element[] = []
  for (const item of items) {
    menuItems.push(<OverlayMenuItem {...item} key={item.label} />)
  }

  return <div className='overlayMenu'>
    {menuItems}
  </div>
}

export default OverlayMenu
