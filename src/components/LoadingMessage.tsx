import React from 'react'

import Spinner from './Spinner'
import './LoadingMessage.css'


/** Time to wait before showing loading message, in milliseconds */
const LOADING_WAIT_TIME = 1000

interface LoadingScreenProps {
  message: string
}

/** Display a loading message with a spinner for long-running-tasks
 *
 * @param message  Text to display with spinner
 */
function LoadingScreen({message}: LoadingScreenProps): JSX.Element | null {
  const [showLoading, setShowLoading] = React.useState(false)
  React.useEffect( () => {
    const timer = setTimeout( () => setShowLoading(true), LOADING_WAIT_TIME)
    return () => clearTimeout(timer)
  }, [])

  // Don't render loading screen until enough time has passed to avoid flashing
  // it for a half-second
  if (!showLoading) {
    return null
  }

  return <div className='loadingScreenContainer'>
    <Spinner color='var(--colorBackgroundText)' diameter='15rem' numThrobbers={8}
      style={{
        marginBottom: '5rem'
      }}
      throbberStyle={{
        width: '5rem',
        height: '1rem',
        borderRadius: '0.25rem'
      }}
    />
    {message}
  </div>
}

export default LoadingScreen
