import { render } from '@testing-library/react'
import React from 'react'

import * as routes from '../routes'
import { renderWithRouter } from '../testutils'

import MainApp from './MainApp'
import MainAppContainer from './MainApp.container'


test.todo('Test route rendering')
test('MainApp container renders', () => {
  render(<MainAppContainer bookIndexUrl='Bookshelf URL' />)
})

test('MainApp component renders', () => {
  renderWithRouter(<MainApp children={null} />, routes.homeUrl)
})
