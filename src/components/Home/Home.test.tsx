import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils'

import { MOCK_FEATURED_COLLECTION } from '../../testdata'
import { renderWithRouter, renderWithTopBar } from '../../testutils'
import { NULL_FUNC } from '../../util'
import Home from './Home'
import HomeContainer from './Home.container'
import HomeCatalogLinker from './HomeCatalogLinker'
import HomeShowcase from './HomeShowcase'

import type { CatalogStatus } from '../hooks/usecatalogindex'


jest.mock('../hooks/usecatalogindex')
jest.mock('../hooks/usestats')

test('HomeContainer tests', () => {
  const loadLocalStub = jest.fn()
  renderWithTopBar(<HomeContainer loadLocalFile={loadLocalStub} />)
})

test('Home renders', () => {
  const indexStatus: CatalogStatus = {
    status: 'loading'
  }
  renderWithTopBar(<Home openLocalFile={NULL_FUNC} showcaseIndexStatus={indexStatus} />)
})

test('HomeCatalogLinker renders', () => {
  renderWithRouter(<HomeCatalogLinker openLocalFile={NULL_FUNC} />)
})

test('HomeShowcase renders', () => {
  mockAllIsIntersecting(false)
  renderWithRouter(<HomeShowcase collection={MOCK_FEATURED_COLLECTION} title='TEST TITLE' />)
})

test.todo('Test opening local file')
