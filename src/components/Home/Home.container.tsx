import React from 'react'

import config from '../../config'
import log from '../../logger'
import useCatalogIndex from '../hooks/usecatalogindex'

import Home from './Home'


export default HomeContainer

/** Orphan <input> element used to load local files */
const FILE_SELECT_ELEMENT = document.createElement('input')
FILE_SELECT_ELEMENT.setAttribute('type', 'file')

interface HomeContainerProps {
  loadLocalFile: (bookFile: File) => void
}

/** Home page for the app, first thing the user sees visiting root
 *
 * @param loadLocalFile: Function to call to load a book from a File blob
 */
function HomeContainer({loadLocalFile}: HomeContainerProps): JSX.Element {
  const showcaseIndex = useCatalogIndex(`${config.dataBaseUrl}/showcase.json`)

  React.useEffect( () => {
    async function loadHandler(ev: Event): Promise<void> {
      const file = FILE_SELECT_ELEMENT.files?.[0]
      if (file) {
        loadLocalFile(file)
      } else {
        log.error('Load file handler called with no files')
      }
    }
    FILE_SELECT_ELEMENT.addEventListener('change', loadHandler)
    return () => {FILE_SELECT_ELEMENT.removeEventListener('change', loadHandler)}
  }, [loadLocalFile])

  return <Home openLocalFile={openLocalFile} showcaseIndexStatus={showcaseIndex} />
}

function openLocalFile(): void {
  FILE_SELECT_ELEMENT.click()
}
