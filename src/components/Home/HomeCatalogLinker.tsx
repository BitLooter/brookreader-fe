import React from 'react'
import { Link } from 'react-router-dom'

import * as routes from '../../routes'
import useStats from '../hooks/usestats'

import './HomeCatalogLinker.css'


interface HomeCatalogLinkerProps {
  openLocalFile: () => void
}

/** Home page for the app, first thing the user sees visiting root */
function HomeCatalogLinker({openLocalFile}: HomeCatalogLinkerProps): JSX.Element {
  const stats = useStats()
  let bookStats
  let statsStatusClass = ''
  if (stats.status === 'error') {
    bookStats = `Error: ${stats.error.message}`
    statsStatusClass = 'error'
  } else if (stats.status === 'uninitialized' || stats.status === 'loading') {
    bookStats = 'Loading library statistics...'
    statsStatusClass = 'loading'
  } else {
    const sizeBooks = Math.round(stats.response.library.booksSize / (1024 ** 2))
    bookStats = `${stats.response.library.booksTotal} books across ${sizeBooks}MB`
  }

  let localLoader
  if (File) {
    localLoader = <div className='homeLinkerLoad' onClick={openLocalFile}>Open a local file</div>
  }

  return <div className='homeCatalogLinker'>
    <Link to={routes.catalogUrl} className='homeLinkerInfo'>
      <div className='homeLinkerInfoTitle'>Browse the full catalog</div>
      <div className={'homeLinkerInfoStats ' + statsStatusClass}>{bookStats}</div>
    </Link>
    {localLoader}
  </div>
}

export default HomeCatalogLinker
