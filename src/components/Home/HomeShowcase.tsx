import React from 'react'
import { Link } from 'react-router-dom'

import { CatalogCollection } from '../../models'
import * as routes from '../../routes'

import CatalogListingRow from '../Catalog/CatalogListingRow'
import '../Catalog/Catalog.css'
import './HomeShowcase.css'


function makeShowcaseBookUrl(collectionId: string, bookId: string): string {
  const key = routes.buildCollectionKey(collectionId, 'showcase')
  return routes.makeBookDefaultPageUrl(key, bookId)
}

interface HomeShowcaseProps {
  collection: CatalogCollection
  title: string
}

/** Display a showcase preview for use on the home page
 *
 * @param collection: Collection object containing the showcase
 * @param title: Title header for showcase
 */
function HomeShowcase({collection, title}: HomeShowcaseProps): JSX.Element {
  const showcaseLink = <Link to={routes.makeShowcaseUrl(collection.id)}>View all</Link>

  return <div className='homeShowcaseContainer'>
    <div className='homeShowcaseHeader'>
      <span className='homeShowcaseHeaderTitle'>{title}</span>
      <span className='homeShowcaseHeaderLink'>{showcaseLink}</span>
    </div>
    <CatalogListingRow
      bookUrlBuilder={makeShowcaseBookUrl}
      collectionUrlBuilder={routes.makeShowcaseUrl}
      collection={collection}
    />
  </div>
}

export default HomeShowcase
