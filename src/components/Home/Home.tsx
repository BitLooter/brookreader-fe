import React from 'react'
import { useHistory } from 'react-router'

import config from '../../config'
import log from '../../logger'
import { CatalogIndex } from '../../models'
import * as routes from '../../routes'
import { CatalogStatus } from '../hooks/usecatalogindex'
import useTopBar from '../hooks/usetopbar'

import { InformationIcon } from '../icons'

import ErrorBox from '../ErrorBox'
import HomeCatalogLinker from './HomeCatalogLinker'
import HomeShowcase from './HomeShowcase'
import './Home.css'


export default Home

interface HomeProps {
  openLocalFile: () => void
  showcaseIndexStatus: CatalogStatus
}

/** Rendering code for root home page
 *
 * @param openLocalFile: Function to call that loads a local File blob
 * @param showcaseIndex: Index for showcase as a download status object
*/
function Home({openLocalFile, showcaseIndexStatus}: HomeProps): JSX.Element {
  const history = useHistory()
  useTopBar(
    null,
    config.siteName,
    {
      icons: [
        {
          action: () => {history.push(routes.aboutUrl)},
          icon: <InformationIcon />,
          label: 'About',
        },
      ]
    }
  )

  let showcase: React.ReactNode
  if (showcaseIndexStatus.status === 'error') {
    showcase = <ErrorBox title='Error loading showcase' message={showcaseIndexStatus.error.message} />
  } else if (showcaseIndexStatus.status !== 'ready') {
    showcase = <div className='homeShowcaseLoading'>
      Loading showcase...
    </div>
  } else {
    showcase = buildShowcases(showcaseIndexStatus.response, config.enabledShowcases)
  }

  return <div className='homeContainer'>
    <HomeCatalogLinker openLocalFile={openLocalFile} />
    {showcase}
  </div>
}

/** Builds showcase displays from a given showcase catalog.
 *
 * @param index: Catalog index from which to source collections
 * @param showcaseIds: Collection IDs to use as showcases
 */
function buildShowcases(index: CatalogIndex, showcaseIds: string[]): React.ReactNode[] {
  const showcases: React.ReactNode[] = []
  for (const id of showcaseIds) {
    // Display error if collection ID not in showcase bookshelf
    if (!(id in index.collections)) {
      const message = `Showcase with ID "${id}" not found`
      log.error(message)
      showcases.push(
        <ErrorBox title='Error with showcase' message={message} key={id} />
      )
      continue
    }

    // Certain IDs get special treatment. Titles will be localized in the future.
    let name
    if (id === 'featured') {
      name = 'Featured'
    } else if (id === 'recent') {
      name = 'Recently added'
    } else {
      name = index.collections[id].name
    }

    showcases.push(
      <HomeShowcase collection={index.collections[id]} title={name} key={id} />
    )
  }
  return showcases
}
