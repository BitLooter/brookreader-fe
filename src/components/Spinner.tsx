import React from 'react'

import './Spinner.css'


// Defaults
const DEFAULT_THROBBER_ANIMATION = 'spinner_fadeout'
const DEFAULT_THROBBER_COLOR = 'white'
const DEFAULT_THROBBER_DIAMETER = '15rem'
const DEFAULT_THROBBER_DURATION = 2
const DEFAULT_THROBBER_TOTAL = 6
const DEFAULT_THROBBER_STYLE: React.CSSProperties = {
  width: '2rem',
  height: '2rem',
  borderRadius: '1rem'
}
const DEFAULT_BOX_STYLE: React.CSSProperties = {}

interface SpinnerProps {
  color?: string
  diameter?: string
  duration?: number
  numThrobbers?: number
  style?: React.CSSProperties
  throbberStyle?: React.CSSProperties
}

/** Generates a spinner suitable for loading messages, consisting of a number
 * of "throbbers" circling a central point.
 *
 * All parameters are optional and will use defaults if not specified.
 *
 * @param color: Color of the throbbers
 * @param diameter: Width of throbber circle in CSS units
 * @param duration: Time in seconds to complete one spin
 * @param numThrobbers: Number of throbbers to generate
 * @param style: CSS style of the containing box
 * @param throbberStyle: CSS style overrides to customize throbbers
 */
function Spinner({color, diameter, duration, numThrobbers, style, throbberStyle}: SpinnerProps): JSX.Element {
  // Insert defaults for undefined values
  const throbberAnimation = DEFAULT_THROBBER_ANIMATION
  const throbberColor = color ?? DEFAULT_THROBBER_COLOR
  const throbberDiameter = diameter ?? DEFAULT_THROBBER_DIAMETER
  const throbberDuration = duration ?? DEFAULT_THROBBER_DURATION
  const throbberStyleOverrides = throbberStyle ?? DEFAULT_THROBBER_STYLE
  const throbberTotal = numThrobbers ?? DEFAULT_THROBBER_TOTAL
  const boxStyle: React.CSSProperties = {
    width: throbberDiameter,
    height: throbberDiameter,
    ...(style ?? DEFAULT_BOX_STYLE)
  }

  const throbbers = []
  for (let throbberIndex = 0; throbberIndex < throbberTotal; throbberIndex++) {
    /** Value between 0.0-1.0 indicating distance around circle */
    const throbberProgress = throbberIndex / throbberTotal
    /** throbberProgress expressed in radians */
    const throbberRadians = Math.PI * 2 * throbberProgress
    /** X coordinate of throbber (as percentage across box) */
    const throbberX = ((Math.cos(throbberRadians) + 1) / 2) * 100
    /** Y coordinate of throbber (as percentage across box) */
    const throbberY = ((Math.sin(throbberRadians) + 1) / 2) * 100
    /** Delay is negative so throbbers are already started on page load */
    const throbberDelay = -(1 - throbberProgress) * throbberDuration

    throbbers.push(
      // Container used to set static properties not affected by animation
      <div
        className='spinnerThrobberContainer'
        style={{
          left: throbberX.toString() + '%',
          top: throbberY.toString() + '%',
          // -50% translate to use throbber center as origin for positioning
          transform: `translate(-50%, -50%) rotate(${throbberProgress}turn)`,
          // Color is var(--throbberColor) by default, throbberStyle may override
          '--throbberColor': throbberColor,
        } as React.CSSProperties}
        key={throbberIndex}
      >
        <div
          className='spinnerThrobber'
          style={{
            animation: `${throbberAnimation} ${throbberDuration}s ${throbberDelay}s infinite`,
            ...throbberStyleOverrides
          }}
        />
      </div>
    )
  }

  return <div className='spinnerBox' style={boxStyle}>
    {throbbers}
  </div>
}

export default React.memo(Spinner)
