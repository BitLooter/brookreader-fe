/** Main entry point for the frontend webapp */

import React from 'react'
import * as ReactDOM from 'react-dom'

import config from './config'
import MainAppContainer from './components/MainApp.container'
import log from './logger'
import * as serviceWorker from './serviceWorker'

import './index.css'


if (config.error) {
  const errorElement = document.getElementById('errorElement')
  if (!errorElement) {
    log.error('Cannot find errorElement')
    throw new Error('Cannot find errorElement')
  }
  errorElement.innerText = config.error.message
  errorElement.style.setProperty('display', 'block')
  log.error(config.error.message)
  throw config.error
}

// Debug panels are hidden by default
document.documentElement.style.setProperty('--debug-panel', 'none')
// Calling debug() on the dev tools console will toggle the debug panel
// eslint-disable-next-line @typescript-eslint/no-explicit-any
;(global as any).debug = () => {
  if (document.documentElement.style.getPropertyValue('--debug-panel') === 'block') {
    document.documentElement.style.setProperty('--debug-panel', 'none')
  } else {
    document.documentElement.style.setProperty('--debug-panel', 'block')
  }
}

// Start rendering
ReactDOM.render(
  <React.StrictMode>
    <MainAppContainer />
  </React.StrictMode>,
  document.getElementById('root') as HTMLElement
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register()
