import { generatePath } from 'react-router-dom'


// Route patterns and URLs
export const bookNoPagePattern = '/book/:collectionKey/:bookId'
export const bookPattern = bookNoPagePattern + '/:pageNum'
export const catalogPattern = '/catalog/:collectionKey?'
export const showcasePattern = '/showcase/:collectionKey?'
export const aboutUrl = '/about'
export const homeUrl = '/'
export const catalogUrl = '/catalog'

// Route-related utility functions
/** Constructs a collection key from parts
 *
 * @param id  ID of the collection
 * @param source  Source of the collection (e.g. showcase), optional
 * @returns  ID/source key for the collection. If source was not given, it will
 *  be identical to the ID.
 */
export function buildCollectionKey(id: string, source?: string): string {
  // @source component of key is optional, if not present defaults to bookshelf
  // file loaded by regular catalog view
  const key = source ? `${id}@${source}` : id
  return key
}

/** Splits collection key into its ID and (optional) source. Essentially the
 * inverse of buildCollectionKey.
 *
 * @param key  Key to split into parts
 * @returns  Tuple of the ID/source pair. If key had no source second element
 *  is undefined.
 * */
export function parseCollectionKey(key: string): [string, string] {
  const [id, source] = key.split('@', 2)
  return [id, source]
}

// Interfaces for route components
export interface BookRouteParams {
  /** ID of the book */
  bookId: string
  /** Key of the collection */
  collectionKey: string
  /** Current page number */
  pageNum: string
}
export interface CatalogRouteParams {
  /** Key of the current collection */
  collectionKey?: string
}

// URL builders
/** Builds URL for book viewer
 *
 * @param collectionKey  ID/source string of the collection containing the book
 * @param bookId  ID of the book
 * @param pageNum  Page number for the book, defaults to 0 if not given
 */
export function makeBookUrl(collectionKey: string, bookId: string, pageNum = 0): string {
  return generatePath(bookPattern, {collectionKey, bookId, pageNum})
}

/** Builds URL for book viewer, omitting page number to use default
 *
 * @param collectionKey  ID/source string of the collection containing the book
 * @param bookId  ID of the book
 */
export function makeBookDefaultPageUrl(collectionKey: string, bookId: string): string {
  return generatePath(bookNoPagePattern, {collectionKey, bookId})
}

/** Build destination URL for closing a book
 *
 * @param collectionKey  ID/source string of the collection containing the book
 * @returns  URL to redirect to on closing book. If a locally loaded book or a
 *  showcase collection, URL will be home.
 */
export function makeCloseBookUrl(collectionKey: string): string {
  const [collectionId, collectionSource] = parseCollectionKey(collectionKey)
  if (collectionId === 'local' || collectionSource === 'showcase') {
    return homeUrl
  } else {
    return makeCatalogUrl(collectionId)
  }
}

/** Builds URL for book catalog
 *
 * @param collectionId  ID of the collection
 */
export function makeCatalogUrl(collectionId: string): string {
  return generatePath(catalogPattern, {collectionKey: collectionId})
}

/** Builds URL for showcase catalog listing
 *
 * @param collectionId  ID of the collection
 */
export function makeShowcaseUrl(collectionId: string): string {
  // Note that showcase URLs should not contain a bookshelf name, only a
  // collection ID. It's named collectionKey in the pattern because both routes
  // are using the same code and expect the same parameters.
  return generatePath(showcasePattern, {collectionKey: collectionId})
}
