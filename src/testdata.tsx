import * as fs from 'fs'

import { CatalogCollection, CatalogIndex, BookInfo } from './models'
import DummyReader from './bookreader/dummyreader'
import { BookViewerContextValue, TopBarContextValue } from './context'
import { BookInfoDetails, CatalogStats } from './models'
import * as routes from './routes'
import { NULL_FUNC } from './util'


// Book/viewer data
export const MOCK_COLLECTION_ID = 'col1'

export const MOCK_BOOK_ID = 'book1'
export const MOCK_BOOK_PAGENUM = 1
export const TEST_BOOK_TITLE = 'Book 1'
export const MOCK_BOOK_URL = routes.makeBookUrl(MOCK_COLLECTION_ID, MOCK_BOOK_ID, MOCK_BOOK_PAGENUM)
/** Book details structure with all data */
export const TEST_BOOK_DETAILS: BookInfoDetails = {
  id: MOCK_BOOK_ID,
  title: TEST_BOOK_TITLE,
  dateAdded: new Date("1970-01-01T00:00:00+00:00"),
  url: MOCK_BOOK_URL,
  coverThumbUrl: 'thumburl',
  coverUrl: 'coverurl'
}
/** JSON representation of a book details structure */
export const MOCK_BOOK_DETAILS_JSON = JSON.stringify(TEST_BOOK_DETAILS)
/** Book details structure with no optional fields */
export const MOCK_BOOK_DETAILS_MINIMUM = {
  id: MOCK_BOOK_ID,
  title: TEST_BOOK_TITLE,
  dateAdded: new Date("1970-01-01T00:00:00+00:00"),
  url: MOCK_BOOK_URL
}
/** JSON representation of a minimal book details structure */
export const MOCK_BOOK_DETAILS_MINIMUM_JSON = JSON.stringify(MOCK_BOOK_DETAILS_MINIMUM)
export const TEST_BOOK_METADATA: BookInfo = {
  coverThumbUrl: 'book1cover-thumb.jpg',
  coverUrl: 'book1cover-full.jpg',
  id: MOCK_BOOK_ID,
  title: TEST_BOOK_TITLE,
}

const TEST_BOOK_MAP = new Map<string, BookInfo>()
TEST_BOOK_MAP.set(TEST_BOOK_METADATA.id, TEST_BOOK_METADATA)
export const TEST_READER_10 = new DummyReader(10)

const MOCK_CBZ_PATH = 'testdata/test.cbz'
export const MOCK_CBZ_DATA = new File([fs.readFileSync(MOCK_CBZ_PATH).buffer], 'test.cbz')


// Collection/catalog data
export const MOCK_CATALOG_URL = routes.makeCatalogUrl(MOCK_COLLECTION_ID)

export const TEST_STATS: CatalogStats = {
  library: {
    booksSize: 0,
    booksTotal: 0
  },
  server: {
    date: new Date("1970-01-01T00:00:00+00:00"),
    version: "0.0.0"
  }
}
export const TEST_STATS_JSON = `{
  "library": {
    "booksSize": 0,
    "booksTotal": 0
  },
  "server": {
    "date": "1970-01-01T00:00:00+00:00",
    "version": "0.0.0"
  }
}`

export const MOCK_BOOKSHELF_JSON = `{
  "info": {
    "name": "Test bookshelf"
  },
  "shelves": {
    "main": {
      "name": "Test main shelf",
      "subshelves": ["${MOCK_COLLECTION_ID}"],
      "books": []
    },
    "${MOCK_COLLECTION_ID}": {
      "name": "Test subshelf",
      "subshelves": [],
      "books": ["${MOCK_BOOK_ID}"]
    }
  },
  "books": {
    "${MOCK_BOOK_ID}": {
      "title": "${TEST_BOOK_TITLE}"
    }
  }
}`

/** Bookshelf file with a self-referential shelf */
export const MOCK_BOOKSHELF_RECURSIVE_JSON = `{
  "info": {
    "name": "Recursive bookshelf",
    "dataBaseUrl": "testurl"
  },
  "shelves": {
    "main": {
      "name": "Recursive shelf",
      "subshelves": ["main"],
      "books": []
    }
  },
  "books": {}
}`

export const MOCK_COLLECTION: CatalogCollection = {
  books: TEST_BOOK_MAP,
  collections: [],
  count: 1,
  coverUrl: 'testurl',
  id: MOCK_COLLECTION_ID,
  name: 'TEST COLLECTION'
}

export const TEST_INDEX: CatalogIndex = {
  collections: {[MOCK_COLLECTION_ID]: MOCK_COLLECTION},
  name: 'TEST INDEX',
  rootCollection: MOCK_COLLECTION
}

export const MOCK_FEATURED_COLLECTION: CatalogCollection = {
  books: TEST_BOOK_MAP,
  collections: [],
  count: 1,
  coverUrl: 'testurl',
  id: 'featured',
  name: 'TEST FEATURED COLLECTION'
}

export const TEST_SHOWCASE_INDEX: CatalogIndex = {
  collections: {featured: MOCK_FEATURED_COLLECTION},
  name: 'TEST SHOWCASE, INDEX',
  rootCollection: MOCK_FEATURED_COLLECTION
}

// Contexts
export const MOCK_VIEWER_CONTEXT: BookViewerContextValue = {
  closeBook: NULL_FUNC,
  closeUrl: '/',
  collectionKey: MOCK_COLLECTION_ID,
  details: TEST_BOOK_DETAILS,
  pageDelta: 0,
  pageNum: MOCK_BOOK_PAGENUM,
  reader: TEST_READER_10,
  setPageNum: NULL_FUNC
}

export const MOCK_TOPBAR_CONTEXT: TopBarContextValue = {
  setHidden: NULL_FUNC,
  setNav: NULL_FUNC,
  setOverlay: NULL_FUNC,
  setup: NULL_FUNC,
}
