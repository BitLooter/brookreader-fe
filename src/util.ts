/** Various utility code that doesn't fit anywhere else */

import config from './config'

export const NULL_BLOB = new Blob()
export const NULL_FUNC = (): null => null
export const VOID_FUNC = (): void => { return }


/** Makes at least on attribute specified in Keys required in T. If no keys are
 * specified all keys are exclusive.
 *
 * Usage examples:
 *  RequireOneOptional<DataSource, 'url' | 'path' | 'stdin'>
 *  RequireOneOptional<UnionType>
 */
export type RequireOneOptional<T, Keys extends keyof T = keyof T> =
  Pick<T, Exclude<keyof T, Keys>> &
  {
    [K in Keys]-?: Required<Pick<T, K>> & Partial<Pick<T, Exclude<Keys, K>>>
  }[Keys]

/** File extensions recognized as valid images */
const VALID_IMAGES = [
  '.jpeg',
  '.jpg',
  '.png',
]

/** Converts a Blob to an ArrayBuffer */
export async function blobToArrayBuffer(data: Blob): Promise<ArrayBuffer> {
  let buffer: ArrayBuffer
  const readPromise = new Promise<ArrayBuffer>( (resolve) => {
    const fileReader = new FileReader()
    fileReader.onloadend = () => {
      buffer = fileReader.result as ArrayBuffer
      resolve(buffer)
    }
    fileReader.readAsArrayBuffer(data)
  })
  await readPromise
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return buffer!  // Known not to be null after promise resolves
}

/** Extract the file extension from a path. Returns '' if no extension. */
export function findExt(path: string): string {
  // Regex matches everything after the final '.' or returns ['', undefined] if
  // no extension. Guaranteed to result in a 2-length array, can skip null check
  const ext = (path.match(/(?:\.([^.]+))?$/) as RegExpMatchArray)[1]
  return ext !== undefined ? ext : ''
}

/**
 * Generates a string from File attributes uniquely identifiying it. Note that
 * a file with the exact same filename, modification date, and size will
 * also have an identical fingerprint.
 */
export function fingerprintFile(file: File): string {
  return file.name + file.size + file.lastModified
}

/** Clamps a value to boundaries */
export function clamp(value: number, min: number, max: number): number {
  return Math.min(Math.max(value, min), max)
}

/** Sets window/tab title with common text. Call without title for default. */
export function setTitle(title?: string): void {
  if (title) {
    document.title = `${title} — ${config.siteName}`
  } else {
    document.title = config.siteName
  }
}

/** Reports if an element is fullscreened */
export function isFullscreen(): boolean {
  return document.fullscreenElement !== null
}

/** Switches between windowed and fullscreen mode */
export function toggleFullscreen(): void {
  if (isFullscreen()) {
    exitFullscreen()
  } else {
    enterFullscreen()
  }
}

/** Set document to fullscreen mode */
export function enterFullscreen(): Promise<void> {
  return document.documentElement.requestFullscreen()
}

/** Set document to windowed mode */
export function exitFullscreen(): void {
  document.exitFullscreen()
}

/** Checks that filename is a valid image type */
export function isImageFile(filename: string): boolean {
  const extensionValid = VALID_IMAGES.some(
    (extension) => filename.toLowerCase().endsWith(extension)
  )
  // Mac OS creates a __MACOSX directory with image metadata, ignore them
  const notMacMeta = !filename.startsWith('__MACOSX')
  return extensionValid && notMacMeta
}

/** Create an URL for a book details resource */
export function makeDetailsUrl(id: string): string {
  return `${config.detailsBaseUrl}/${id}.json`
}

/** Create an URL for a book cover */
export function makeCoverUrl(id: string): string {
  return `${config.coversBaseUrl}/${id}-full.jpg`
}

/** Create an URL for a book cover thumbnail */
export function makeCoverThumbUrl(id: string): string {
  return `${config.coversBaseUrl}/${id}-thumb.jpg`
}
