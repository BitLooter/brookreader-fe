import LoaderBase from './loaderbase'


const NULL_FILE = new File([], '')

export default class DummyLoader implements LoaderBase {
  public readonly loaded = true
  public readonly filePaths: string[] = []

  public async getFileBlob(_path: string): Promise<File> {
    return NULL_FILE
  }

  public async getFileUrl(_path: string): Promise<string> {
    return ""
  }

  public get archiveType(): string {
    return "dummy"
  }
}
