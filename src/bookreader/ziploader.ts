/** Loader code for ZIP archives */

import { Zipcord, errors as ZipcordErrors } from 'zipcord'

import { BookSourceError, NotLoadedError } from '../errors'
import { makeLogger } from '../logger'
import LoaderBase from './loaderbase'


const log = makeLogger('Loader (zip)')

/** Streaming data loader for ZIP/CBZ files */
class ZipLoader extends LoaderBase {
  /** ZipStream object files are loaded from */
  protected zip?: Zipcord

  /** Creates and initiializes a ZipLoader from an URL */
  public static async openUrl(url: string): Promise<ZipLoader> {
    log.info(`Loading streaming zip from URL: ${url}`)

    const loader = new ZipLoader()
    await loader.loadSource(url)
    return loader
  }

  /** Creates and initiializes a ZipStreamLoader from a Blob */
  public static async openBlob(data: Blob): Promise<ZipLoader> {
    log.info(`Loading zip from Blob (Size: ${data.size})`)

    const loader = new ZipLoader()
    await loader.loadSource(data)
    return loader
  }

  protected async loadSource(source: string | Blob): Promise<void> {
    try {
      this.zip = await Zipcord.open(source)
    } catch (error) {
      if (error instanceof ZipcordErrors.ZipError) {
        throw new BookSourceError(error.message)
      } else {
        throw error
      }
    }

    if (typeof source === 'string') {
      this.sourceUrl = source
    }

    this.filePaths = Array.from(this.zip.entries.keys())
    this.loaded = true
  }

  // Abstract class implementations

  public async getFileBlob(path: string): Promise<File> {
    if (!this.zip) {
      throw new NotLoadedError('Attempted file read before Zip finished loading')
    }
    // localHeader was loaded in previous call
    const entry = this.zip.entries.get(path)
    if (!entry) {
      throw new ReferenceError(
        `Archive ${this.sourceUrl ?? '<local file>'} does not have an entry for ${path}`
      )
    }
    return entry.blob()
  }

  public async getFileUrl(path: string): Promise<string> {
    return window.URL.createObjectURL(await this.getFileBlob(path))
  }

  public get archiveType(): string {
    return "zip"
  }
}

export default ZipLoader
