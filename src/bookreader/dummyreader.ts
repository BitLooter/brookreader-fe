/** Dummy/placeholder book reader */

import ReaderBase, { PageInfo } from './readerbase'
import DummyLoader from './dummyloader'


/** BookReader implementation that doesn't do anything, used as a placeholder
 * until a real book is loaded and for testing
 */
class DummyReader extends ReaderBase {
  constructor(readonly pages = 1) {
    super(new DummyLoader())
  }

  public cleanup(): void {}

  public getPageInfo(_pageNum: number): PageInfo {
    return {
      extension: 'dummy',
      filename: 'dummy'
    }
  }

  protected loadPageImageUrl(_pageNum: number): Promise<string> {
    return Promise.resolve("")
  }

  get totalPages(): number {
    return this.pages
  }
}

export const dummyReader = new DummyReader()

export default DummyReader
