/** Loader code for bare directories of files */

import { BookSourceError } from '../errors'
import { makeLogger } from '../logger'
import LoaderBase from './loaderbase'


const log =  makeLogger('Loader (dir)')

/** Data loader for bare directories */
class DirLoader extends LoaderBase {
  /** Creates and initiializes a DirLoader from an URL */
  public static async openUrl(url: string, pageUrls?: string[]): Promise<DirLoader> {
    log.info(`Loading bare directory at URL: ${url}`)

    if (!pageUrls) {
      throw new Error('DirLoader requires pageUrls to be passed')
    }

    const loader = new DirLoader()
    loader.filePaths = pageUrls
    loader.loaded = true
    return loader
  }

  /** Invalid action, throw an error if this ever happens */
  public static async openBlob(_: Blob): Promise<DirLoader> {
    throw new BookSourceError('Cannot open Blob as bare directory')
  }

  // Abstract class implementations

  public async getFileBlob(path: string): Promise<File> {
    log.debug(`Fetching page ${path}`)
    const response = await fetch(path)
    const blob = await response.blob()

    return new File([blob], path, {
      type: response.headers.get('Content-Type') ?? undefined,
      lastModified: response.headers.has('Date') ?
        Date.parse(response.headers.get('Date') as string) :
        undefined
    })
  }

  public async getFileUrl(path: string): Promise<string> {
    // File paths are the URLs in DirLoader
    // Start downloading URL so it's in the browser cache
    fetch(path)
    return path
  }

  public get archiveType(): string {
    return "dir"
  }
}

export default DirLoader
