/** Reader code for image-slideshow-based books */

import { BookSourceError, NotLoadedError } from '../errors'
import log from '../logger'
import { isImageFile } from '../util'
import ReaderBase from './readerbase'
import LoaderBase from './loaderbase'


/** An URL representing the page or an Error generating while loading page.
 * Errors are stored and not thrown right away, so they can be thrown
 * (potentially multiple times) when retrieving the page from the cache. */
type PageOrError = string | Error
/** Cache maps image filename paths to Page URLs or Errors */
interface PageCache {
  [imagePath: string]: Promise<PageOrError>
}
interface PageInfo {
  extension: string
  filename: string
}

const LOG_LABEL = 'Reader (Comic)'

/** Reader implementation for comic book archives (CBZ, CBR, etc.) */
export default class ComicReader extends ReaderBase {
  protected cache: PageCache = {}
  protected imagePaths: string[] = []

  constructor(protected readonly src: LoaderBase) {
    super(src)
    this.imagePaths = src.filePaths.filter( isImageFile ).sort()
    if (this.imagePaths.length === 0) {
      throw new BookSourceError('No images in ComicReader source')
    }
  }

  public cleanup(): void {
    log.debug('Cleaning up reader', LOG_LABEL)
    for (const page in Object.values(this.cache)) {
      window.URL.revokeObjectURL(page)
    }
    this.cache = {}
  }

  public getPageInfo(pageNum: number): PageInfo {
    return {
      // Cast to string, split() guaranteed to return string here
      extension: this.imagePaths[pageNum].split('.').pop() as string,
      filename: this.imagePaths[pageNum]
    }
  }

  protected async loadPageImageUrl(pageNum: number): Promise<string> {
    // Loader status was checked in constructor, should already be available
    // for loading pages or this method will fail

    // Method does not load image directly, but rather updates the cache and
    // return results from there
    this.updateCache(pageNum)

    const filePath = this.imagePaths[pageNum]
    if (!(filePath in this.cache)) {
      // updateCache should have populated filePath in the cache
      throw new NotLoadedError(`Page ${pageNum} (${filePath}) not in cache`)
    }
    // Wait for file to finish loading so we know can throw if it's an error
    const pageOrError = await this.cache[filePath]
    if (typeof pageOrError === 'string') {
      return pageOrError
    } else {
      throw pageOrError
    }
  }

  /** Refresh the page cache based on the current page number */
  protected updateCache(pageNum: number): void {
    const pagesToCache = [pageNum, pageNum + 1].filter((num) =>
      num >= 0 && num < this.totalPages)
    for (const cachePageNum of pagesToCache) {
      const cachePagePath = this.imagePaths[cachePageNum]
      if (!(cachePagePath in this.cache)) {
        log.debug('Caching page ' + cachePagePath, LOG_LABEL)

        // Capture any thrown errors during loading to use on retrieval later
        const cacheThisPage = async (): Promise<PageOrError> => {
          try {
            return this.src.getFileUrl(cachePagePath)
          } catch (e) {
            return e
          }
        }
        this.cache[cachePagePath] = cacheThisPage()
      }
    }
  }

  get totalPages(): number {
    return this.imagePaths.length
  }
}
