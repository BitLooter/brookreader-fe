import * as crypto from 'crypto'

import { BookSourceError } from '../errors'
import { MOCK_CBZ_DATA } from '../testdata'
import { mockFetch, unmockFetch } from '../testutils'
import { blobToArrayBuffer } from '../util'
import ComicReader from './comicreader'
import DummyReader from './dummyreader'
import DummyLoader from './dummyloader'
import loadBookReader from './index'
// import RarLoader from './rarloader'
import ZipLoader from './ziploader'

import type { ILoaderFactories } from './index'
import type LoaderBase from './loaderbase'


const TEST_BOOK_LENGTH = 3
// const TEST_CBR_PATH = 'testdata/test.cbr'
// const TEST_CBR_DATA = new Blob([fs.readFileSync(TEST_CBR_PATH).buffer])
const TEST_IMAGE_NAME = '1.png'
const TEST_IMAGE_SHA256 = 'f9a395f2918555696bfb73d812658af1cd5a853f0e9059cece467609a5aeef92'
const TEST_LOADER = new DummyLoader()

const readerTestParams = [
  // Name,Loader,          Data,          Filename,       Bad filename
  ['ZIP', ZipLoader, MOCK_CBZ_DATA, 'filename.cbz', 'wrong.cbr'],
  // ['RAR', RarLoader,       MOCK_CBR_DATA, 'filename.cbr', 'wrong.cbz'],
]
// Types of function arguments test.each() uses with readerTestParams
type ReaderTestParamsTypes = [string, ILoaderFactories, Blob, string, string]


// The tests
/////////////

afterEach(() => unmockFetch())

async function commonLoaderChecks(loader: LoaderBase): Promise<void> {
  expect(loader.loaded).toBeTruthy()

  const hash = crypto.createHash('sha256')
  hash.update(
    Buffer.from(
      await blobToArrayBuffer(
        await loader.getFileBlob(TEST_IMAGE_NAME))))
  expect(hash.digest('hex')).toEqual(TEST_IMAGE_SHA256)

}
test.todo('Corrupt zip, cbr errors')
test.todo('Hash check on data')

test.each<ReaderTestParamsTypes>(readerTestParams)(
  'Loader from Blob (%s)',
  async (_, loaderClass, data
) => {
  const loader = await loaderClass.openBlob(data)

  commonLoaderChecks(loader)
})
test.todo('Test error conditions')

test.each<ReaderTestParamsTypes>(readerTestParams)(
  'Loader from URL (%s)',
  async (_, loaderClass, data, filename
) => {
  mockFetch(data)
  const loader = await loaderClass.openUrl('https://example.com/' + filename)

  commonLoaderChecks(loader)
})

test.each<ReaderTestParamsTypes>(readerTestParams)(
  'Comic reader (%s)',
  async (_, loaderClass, data
) => {
  const loader = await loaderClass.openBlob(data)
  const reader = new ComicReader(loader)

  // Attributes
  expect(reader.totalPages).toEqual(TEST_BOOK_LENGTH)

  // Page numbers
})
test.todo('Should throw exception if out of range? Is there any reason to allow that behavior?')
test.todo('Hash check on data for this test')

test('Dummy reader', () => {
  const reader = new DummyReader()

  expect(reader.totalPages).toEqual(1)
})

// Test disabled until the dummy loader can do dummy filenames
test.skip(
// test.each<ReaderTestParamsTypes>(readerTestParams)(
  'Autodetecting factory with %s',
  async (_, loaderClass, data, filename, wrongname
) => {
  const mockOpenBlob = jest.spyOn(loaderClass, 'openBlob')
    .mockName('openBlob')
    .mockImplementation(async () => TEST_LOADER)
  const mockOpenUrl = jest.spyOn(loaderClass, 'openUrl')
    .mockName('openUrl')
    .mockImplementation(async () => TEST_LOADER)

  await loadBookReader(data, {filename})
  expect(mockOpenBlob).toBeCalled()
  await loadBookReader('https://example.com/' + filename)
  expect(mockOpenUrl).toBeCalled()

  // loadBookReader requires a filename if givin a Blob
  await expect(loadBookReader(data)).rejects.toThrow(BookSourceError)
  // Test loading the wrong file type. Note that loadBookReader will use the
  // filename to determine loader type, so e.g the CBR test will throw an error
  // from the ZIP loader.
  await expect(loadBookReader(data, {filename: wrongname})).rejects.toThrow(BookSourceError)
  // Invalid/unknown file extension
  await expect(loadBookReader(data, {filename: 'fake.extension'})).rejects.toThrow(BookSourceError)

  jest.restoreAllMocks()
})
test.todo('Corrupt/invalid archives')
