/** Loader code for RAR archives */

import { Archive, ExtractedFilesObject } from 'libarchive.js/main.js'

import { BookSourceError, InternalError, NotLoadedError } from '../errors'
import log from '../logger'
import LoaderBase from './loaderbase'

// Libarchive.js needs to know where its service worker is. Downloaded when
// an archive is opened. libarchive contents needs to be updated when
// libarchive.js is updated.
Archive.init({workerUrl: 'libarchive/worker-bundle.js'})


const LOG_LABEL = 'Loader (rar)'
/** Data loader for RAR/CBR files */
export default class RarLoader extends LoaderBase {
  /** Files extracted from RAR archive */
  protected files: Map<string, File> = new Map()

  /** Creates and initiializes a RarLoader from an URL */
  public static async openUrl(url: string): Promise<RarLoader> {
    log.info(`Loading rar from URL: ${url}`, LOG_LABEL)

    const response = await fetch(url)
    const data = await response.blob()
    const loader = new RarLoader()
    loader.sourceUrl = url
    await loader.loadRar(data)
    return loader
  }

  /** Creates and initiializes a RarLoader from a Blob */
  public static async openBlob(data: Blob): Promise<RarLoader> {
    log.info(`Loading rar from Blob (Size: ${data.size})`, LOG_LABEL)

    const loader = new RarLoader()
    await loader.loadRar(data)
    return loader
  }

  /** Download and extract contents of RAR archive */
  protected async loadRar(content: Blob): Promise<void> {
    const archive = await Archive.open(content)
    // TODO:LIBARCHIVE: libarchive.js has a bug where it extracts the wrong
    // file sometimes when calling extract() from a CompressedFiles object.
    // Seems to be a race condition when extracting more than one file at a
    // time? Viewer currently extracts more than one file at a time while
    // precaching so all files need to be extracted upfront til fixed.
    // TODO:LIBARCHIVE: File a bug report for above
    // TODO:LIBARCHIVE: Also one about the API changing after extractFiles() is called
    const extractedFiles = await archive.extractFiles()

    // Libarchive.js does not throw errors on invalid archives, check that any
    // entries were found in file
    if (Object.keys(extractedFiles).length === 0) {
      throw new BookSourceError('Could not open file as RAR archive')
    }

    const buildOutput = (extractObject: ExtractedFilesObject, currentPath = ''): void => {
      for (const [path, obj] of Object.entries(extractObject)) {
        if (obj instanceof File) {
          this.files.set(`${currentPath}/${obj.name}`, obj)
        } else {
          buildOutput(obj, path)
        }
      }
    }
    buildOutput(extractedFiles)

    this.filePaths = Array.from(this.files.keys())
    this.loaded = true
  }

  // Abstract class implementations
  //////////////////////////////////

  public async getFileBlob(path: string): Promise<File> {
    if (!this.loaded) {
      throw new NotLoadedError('Attempted file read before Rar finished loading')
    }

    const file = this.files.get(path)
    if (!file) {
      throw new InternalError(`Path '${path}' not found in archive ${this.sourceUrl}`)
    }

    return file
  }

  public async getFileUrl(path: string): Promise<string> {
    if (!this.loaded) {
      throw new NotLoadedError('Attempted file read before Rar finished loading')
    }

    return window.URL.createObjectURL(await this.getFileBlob(path))
  }

  public get archiveType(): string {
    return "rar"
  }
}
