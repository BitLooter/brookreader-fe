/* Common code for data loaders */

/** Loads data from an underlying archive or URLs */
export default abstract class LoaderBase {
  /** Set to true when ready to read files */
  public loaded = false
  /** Array of filenames available to be loaded */
  public filePaths: string[] = []
  /** URL files are loaded from, if applicable */
  public sourceUrl?: string

  /** Retrieve a file from the loader, as Blob */
  public abstract getFileBlob(path: string): Promise<File>
  /** Retrieve a file from the loader, as an URL. May convert a Blob to an URL
   * before returning. */
  public abstract getFileUrl(path: string): Promise<string>
  public get archiveType(): string {
    return "none"
  }
}
