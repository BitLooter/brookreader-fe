/** Common interface for opening book files. You shouldn't normally need to
 * import readers/loaders directly, loadBookReader is a factory function that
 * handles all that automatically.
 */

import { BookSourceError } from '../errors'
import { findExt } from '../util'
import ReaderBase from './readerbase'
import ComicReader from './comicreader'
import DirLoader from './dirloader'
import LoaderBase from './loaderbase'
import RarLoader from './rarloader'
import ZipLoader from './ziploader'


export { default as ReaderBase,
         BookContentType } from './readerbase'

/** Union of all types that the extra field could hold */
type LoaderExtra = string[]

interface LoaderFactories {
  openBlob: (f: Blob) => Promise<LoaderBase>
  openUrl: (f: string, extra?: LoaderExtra) => Promise<LoaderBase>
}

interface OpenerMapping {
  [type: string]: [new (src: LoaderBase) => ReaderBase, LoaderFactories]
}

const openerTypes: OpenerMapping = {
  cbr: [ComicReader, RarLoader],
  cbz: [ComicReader, ZipLoader],
  dir: [ComicReader, DirLoader],
  rar: [ComicReader, RarLoader],
  zip: [ComicReader, ZipLoader],
}

/*
*  filename: File name of the book being loaded
*
*/
interface LoadReaderOptions {
  filename?: string
  type?: string
  extra?: LoaderExtra
}

/** Determines correct loader/reader types based on input and handles
 * initialization. Takes an options argument that affects loading. Note that
 * either filename or type must be provided to load Blobs.
 *
 * Options parameters:
 *  filename: File name of the book being loaded
 *  type: Type of the book being loaded. Autodetected from filename or source
 *        (URL). May be needed if filename/URL doesn't have the correct
 *        extension. Use 'dir' for bare directories.
 *  extra: Second parameter passed to the loader (e.g. bare directory URLs)
 */
async function loadBookReader(source: string | Blob,
                              options?: LoadReaderOptions): Promise<ReaderBase> {
  const {filename, type, extra} = (options ?? {}) as LoadReaderOptions
  if (source instanceof Blob && !(filename || type)) {
    // Not enough information to determine type
    throw new BookSourceError('You must provide a filename or type for loading blobs')
  }

  // From above check we know type or filename must be set if source is a string
  const bookFormat = type ? type : findExt(filename ?? source as string).toLowerCase()
  if (!(bookFormat in openerTypes)) {
    throw new BookSourceError('Unknown book format: ' + bookFormat)
  }
  const [Reader, Loader] = openerTypes[bookFormat]
  let loader: LoaderBase
  if (typeof source === 'string') {
    // Loading from an URL
    loader = await Loader.openUrl(source, extra)
  } else if (source instanceof Blob) {
    // Loading from a local file
    loader = await Loader.openBlob(source)
  } else {
    // In theory TypeScript should never allow this to happen
    throw new TypeError('Unknown source type for book loader')
  }

  return new Reader(loader)
}

export default loadBookReader
