/** Common code for all book readers */

import { NotLoadedError } from '../errors'
import log from '../logger'
import LoaderBase from './loaderbase'


// Babel does not support const enum
// https://github.com/babel/babel/issues/8741
/** Format of the book's content, e.g. image slideshow or text */
export enum BookContentType {
  Image
}

export interface PageInfo {
  extension: string
  filename: string
}


/** Base class for book readers */
abstract class ReaderBase {
  public bookType: BookContentType = BookContentType.Image

  constructor(protected readonly src: LoaderBase) {
    if (!src.loaded) {
      throw new NotLoadedError('Loader not loaded, cannot build reader')
    }
  }

  /** Retrieve URL for the page with the given number. Source of the URL is
   * defined by the Reader implementation and Loader source, and may involve
   * caching and converting Blobs. */
  public async getPageUrl(pageNum: number): Promise<string> {
    log.debug(`Getting URL for page ${pageNum}`, 'ReaderBase')

    // Check that the page number is valid
    const lastPage = this.totalPages - 1
    if (0 > pageNum || pageNum > lastPage) {
      throw new RangeError(`Page ${pageNum} outside valid range 0-${lastPage}`)
    }

    return this.loadPageImageUrl(pageNum)
  }

  /** Type of the underlying Loader source */
  public get archiveType(): string {
    return this.src.archiveType
  }

  // Abstract methods

  /** Reader cleanup, e.g. revoking object URLs */
  public abstract cleanup(): void
  /** Get information about a specific page, e.g. filename */
  public abstract getPageInfo(pageNum: number): PageInfo
  /** Implementation-specific page loading code */
  protected abstract loadPageImageUrl(pageNum: number): Promise<string>
  /** Total number of pages available to the Reader */
  abstract get totalPages(): number
}

export default ReaderBase
