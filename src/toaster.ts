/** Implementation of 'Toast' notifications
 *
 * Unlike the rest of the app, toasts are not rendered with React. Initial
 * attempts at this were to create a HOC that wrapped the entire app and
 * provided a context for the sendToast() function, but this ended up having
 * issues with code that needs to send toasts on startup (e.g. reporting
 * redirecting from an invalid link to the user) - components would attempt to
 * send a toast before everything finishes rendering and they would simply
 * disappear. This is a solvable problem, of course, but as the structure of
 * toasts are very simple (literally a single <div> in the initial version as I
 * write this) it ended up cutting out a lot of complexity to render them this
 * way.
 *
 * If for some reason I ever want to render JSX in toasts it won't be
 * difficult to simply treat toastElement as an additional ReactDOM render()
 * root. As long as they are little more than static text messages that level
 * of complexity remains unneccessary.
 */

import './toaster.css'


export default sendToast

/** Time to display toast in milliseconds */
const TOAST_DURATION = 5000

const toastElement = document.createElement('div')
toastElement.classList.add('toasterSlice')
const toastTextNode = document.createTextNode('NO BREAD')
toastElement.append(toastTextNode)
document.body.append(toastElement)

/** Display a toast notification.
 *
 * Any toasts sent before the previous finishes will replace the text but not
 * the timeout. If I ever need to send multiple toasts in a short time I'll
 * need to build a queueing system for them.
 */
function sendToast(message: string): void {
  toastTextNode.nodeValue = message
  toastElement.classList.add('toasterSliceVisible')
  setTimeout( () => toastElement.classList.remove('toasterSliceVisible'), TOAST_DURATION)
}

// Allow sending toasts from browser console for testing
// eslint-disable-next-line @typescript-eslint/no-explicit-any
(globalThis as any).debugToast = sendToast
