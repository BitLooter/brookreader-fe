/** Interfaces and types used by the application */

import { RequireOneOptional } from './util'


/** Minimal book information, used in catalog */
export interface BookInfo {
  coverThumbUrl: string
  coverUrl: string
  id: string
  title: string
}

/** Full details on a book. Defaults may be filled in (e.g. cover URLs) */
export interface BookInfoDetails extends BookInfo {
  dateAdded: Date
  url: string
  nextId?: string
  prevId?: string
  pageUrls?: string[]
}

type BookMap = Map<string, BookInfo>
/** Collection of books used by the catalog. Represented as a tree of
 * collections and generated from a bookshelf file. */
export interface CatalogCollection {
  books: BookMap
  collections: CatalogCollection[]
  coverUrl: string
  count: number
  id: string
  name: string
  parent?: CatalogCollection
}

/** Object pairing collection IDs to collections. Probably should be a Map. */
export interface CatalogIndexCollectionsObject {[collectionId: string]: CatalogCollection}

/** Top-level container for catalog collections */
export interface CatalogIndex {
  collections: CatalogIndexCollectionsObject
  name: string
  rootCollection: CatalogCollection
}

/** Statistics about the book database */
export interface CatalogStats {
  library: {
    booksSize: number
    booksTotal: number
  }
  server: {
    date: Date
    version: string
  }
}

/** Download has not been started. Usually only for initial state values. */
interface DownloadStatusUninitialized {
  status: 'uninitialized'
}
/** Download is currently downloading */
interface DownloadStatusLoading {
  status: 'loading'
}
/** DOwnload is complete */
interface DownloadStatusReady<T> {
  status: 'ready'
  response: T
}
/** Error occured while downloading */
interface DownloadStatusError {
  status: 'error'
  error: Error
}
/** Union of all download status interfaces. Value of status field can be used
 * to narrow status type.
 */
export type DownloadStatus<T> = DownloadStatusUninitialized |
  DownloadStatusLoading | DownloadStatusReady<T> | DownloadStatusError

/** Based class representing a action to take for elements that present a
 * user command, for example a menu item or button. Typically paired with
 * RequireOneOptional to force a choice.
 *
 * Of action, route, and url, only one should be specified. */
interface UserCommandPropertiesFull {
  /** Text label for the command */
  label: string
  /** Optional JSX element used as icon, typically an svg or img */
  icon?: JSX.Element
  /** Function to execute on command */
  action?: () => void
  /** Internal route redirection */
  route?: string
  /** Destination URL of link */
  url?: string
}
type CommandExclusiveKeys = 'action' | 'route' | 'url'
/** Prebaked command properties type allowing to specify label, icon, and one
 * of action, route, and url. */
export type UserCommandProperties = RequireOneOptional<
  UserCommandPropertiesFull, CommandExclusiveKeys
>
