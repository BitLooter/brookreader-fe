// TODO: Tests need to be overhauled and fixed

import { cleanup } from '@testing-library/react'
import { TextDecoder } from 'util'


// eslint-disable-next-line @typescript-eslint/no-explicit-any
; (global as any).TextDecoder = TextDecoder
window.URL.createObjectURL = () => ''

// Mock matchMedia
// https://jestjs.io/docs/en/manual-mocks#mocking-methods-which-are-not-implemented-in-jsdom
Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: jest.fn().mockImplementation((query) => ({
    matches: false,
    media: query,
    onchange: null,
    addListener: jest.fn(), // deprecated
    removeListener: jest.fn(), // deprecated
    addEventListener: jest.fn(),
    removeEventListener: jest.fn(),
    dispatchEvent: jest.fn(),
  }))
})

afterAll(cleanup)
