import { makeBookUrl } from './routes'
import { MOCK_BOOK_ID, MOCK_BOOK_PAGENUM, MOCK_COLLECTION_ID } from './testdata'


test('Building book route', () => {
  expect(makeBookUrl(MOCK_COLLECTION_ID, MOCK_BOOK_ID, MOCK_BOOK_PAGENUM)).toEqual('/book/col1/book1/1')
  expect(makeBookUrl(MOCK_COLLECTION_ID, MOCK_BOOK_ID)).toEqual('/book/col1/book1/0')
})
