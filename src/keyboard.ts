/** Manages keyboard input */

interface Keymap {
  [key: string]: () => void
}

let keymap: Keymap = {}

document.addEventListener('keydown', (event: KeyboardEvent) => {
  if (event.key in keymap) {
    keymap[event.key]()
  }
})

function registerKeyBindings(newKeymap: Keymap): void {
  keymap = newKeymap
}

export default registerKeyBindings
