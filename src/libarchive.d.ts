// Typings file is temporary until it can be added to DefinitelyTyped

declare module 'libarchive.js/main.js' {
  export interface ArchiveOptions {
    workerUrl: string
  }

  export interface AchiveFilesObject {
    // getFilesObject() returns a hierarchical data structure that uses File or
    // CompressedFile depending on if extractFiles() was called
    [filename: string]: AchiveFilesObject | CompressedFile | File
  }
  export interface ArchiveFilesArrayItem {
    // getFilesArray() returns CompressedFile until extractFiles is called,
    // then it returns File
    file: CompressedFile | File
    path: string
  }
  export class CompressedFile {
    public async extract(): Promise<File>
    public name: string
    public size: number
  }
  type ExtractCallback = (extractedFile: {file: File; path: Path}) => void
  export interface ExtractedFilesObject {
    [filename: string]: File | ExtractedFilesObject
  }

  export class Archive {
    static init(options: ArchiveOptions = {}): ArchiveOptions
    static async open(file: Blob, options: ArchiveOptions | null = null): Promise<Archive>
    close(): void
    hasEncryptedData(): boolean | null
    usePassword(archivePassword: string): Promise<boolean>
    async getFilesObject(): Promise<AchiveFilesObject>
    async getFilesArray(): Promise<ArchiveFilesArrayItem[]>
    async extractSingleFile(path: string): Promise<File>
    async extractFiles(extractCallback?: ExtractCallback): Promise<ExtractedFilesObject>
  }
}
