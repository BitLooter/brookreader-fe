/** Console logging code. Use this instead of calling console.log directly */

type LogLevels = 'off' | 'error' | 'warning' | 'info' | 'debug'
// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ConsoleLog = (message?: any, ...optionalParams: any[]) => void
interface Logger {
  debug: (msg: string, label?: string) => void
  dump: (obj: unknown) => void
  error: (msg: string, label?: string) => void
  info: (msg: string, label?: string) => void
  raw: ConsoleLog
  warning: (msg: string, label?: string) => void
}

const LABEL_CSS_COMMON = 'font-weight: bold; border-radius: 1em;'
const LEVELS: {[levelName: string]: {rank: number; css: string; labelCss: string}} = {
  off: {
    rank: 0,
    css: 'Should never be rendered',
    labelCss: ''
  },
  error: {
    rank: 1,
    css: 'font-style: bold; color: white; background-color: red',
    labelCss: LABEL_CSS_COMMON + 'color: white; background-color: red'
  },
  warning: {
    rank: 2,
    css: 'font-style: italic; color: red',
    labelCss: LABEL_CSS_COMMON + 'color: white; background-color: maroon'
  },
  info: {
    rank: 3,
    css: '',
    labelCss: LABEL_CSS_COMMON + 'color: black; background-color: lightgrey'
  },
  debug: {
    rank: 4,
    css: 'font-style: italic; color: darkgrey',
    labelCss: LABEL_CSS_COMMON + 'color: lightgrey; background-color: darkslategrey'
  },
}

let logLevel: LogLevels = process.env.NODE_ENV !== 'test' ? 'debug' : 'off'
export function setLogLevel(level: LogLevels): void {
  logLevel = level
}
// Add to global so log level can be easily set in console
// eslint-disable-next-line @typescript-eslint/no-explicit-any
(globalThis as any).setLogLevel = setLogLevel

function filteredLog(level: LogLevels, message: string, logFunc: ConsoleLog, label?: string): void {
  const prefix = label ? `%c ${label} %c ` : '%c%c'
  if (LEVELS[level].rank <= LEVELS[logLevel].rank ) {
    logFunc(prefix + message, LEVELS[level].labelCss, LEVELS[level].css)
  }
}


/* eslint-disable no-console */
/** Creates a logger with the given label as default */
export function makeLogger(label?: string): Logger {
  return {
    debug: (msg, thisLabel) => filteredLog('debug', msg, console.log, thisLabel ?? label),
    dump: (obj) => console.table(obj),
    error: (msg, thisLabel) => filteredLog('error', msg, console.error, thisLabel ?? label),
    info: (msg, thisLabel) => filteredLog('info', msg, console.log, thisLabel ?? label),
    raw: console.log,
    warning: (msg, thisLabel) => filteredLog('warning', msg, console.warn, thisLabel ?? label),
  }
}
/* eslint-enable no-console */

export default makeLogger()
