import {
  MOCK_BOOKSHELF_JSON, MOCK_BOOKSHELF_RECURSIVE_JSON, MOCK_COLLECTION_ID
} from '../testdata'
import { mockFetch, unmockFetch } from '../testutils'
import fetchBookIndex from './catalogindex'


afterEach(() => unmockFetch())

test('Loading bookshelf file', async () => {
  mockFetch(MOCK_BOOKSHELF_JSON)
  const index = await fetchBookIndex('Bookshelf URL')

  expect(index.name).toBe('Test bookshelf')
  expect(index.rootCollection.id).toBe('main')
  expect(Object.keys(index.collections)).toHaveLength(2)
  expect(index.collections).toHaveProperty(MOCK_COLLECTION_ID)
})

// Test is currently disabled until I can figure out whether Jest is stupid or
// I am. mockFetch is not working here, it's reusing the return value from the
// above test after manually calling unmockFetch directly before mockFetch
// here, or even after manually setting window.fetch to anything else. I also
// tried using jest.mock() to mock the implementation of fetchJson with similar
// results. No matter what I do Jest insists on reusing mocked data from the
// first test, and after spending two hours pouring over documentation and
// StackOverflow just trying to get it to use different data in a different
// test I am sick of this and throwing in the towel. Maybe I'll revisit this
// again in the future. Maybe I'll just shove red-hot needles under my
// toenails instead, the pain would be comparable and I'd save a lot of time.
test.skip('Recursive shelf', async () => {
  mockFetch(MOCK_BOOKSHELF_RECURSIVE_JSON)
  const index = await fetchBookIndex('Bookshelf URL')

  expect(index.rootCollection.id).toBe('main')
  expect(Object.keys(index.collections)).toHaveLength(1)
  expect(index.rootCollection.collections[0].id).toBe('main')
  // Collection at maximum depth should have no subshelves
  expect(index.rootCollection
    .collections[0].collections[0].collections[0].collections[0].collections[0]
    .collections[0].collections[0].collections[0].collections[0].collections[0]
    .collections[0].collections[0].collections[0].collections[0].collections[0]
    .collections[0].collections[0].collections[0].collections[0].collections[0]
    .collections
  ).toHaveLength(0)
})

test.todo('Schema tests')
test.todo('Connection errors')
test.todo('Corrupt bookshelves')
