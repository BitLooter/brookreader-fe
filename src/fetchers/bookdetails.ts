/** Download and process book details files */

import log from '../logger'
import { BookInfoDetails } from '../models'
import { makeDetailsUrl, makeCoverThumbUrl, makeCoverUrl } from '../util'
import { fetchJson } from '.'


type BookInfoDetailsMinimum = Omit<BookInfoDetails, "coverThumbUrl"|"coverUrl">
/** Represents details type before generated data is added */
interface BookInfoDetailsJson extends BookInfoDetailsMinimum {
  coverThumbUrl?: string
  coverUrl?: string
}

/** Simple cache for details objects persisting until reloading */
// const detailsSessionCache: {[id: string]: IBookInfoDetails} = {}
const detailsSessionCache = new Map<string, BookInfoDetails>()

/** Fetches details object for a given book ID. Also inserts default
 * information in optional fields, e.g. coverUrl. */
export default async function fetchBookDetails(bookId: string): Promise<BookInfoDetails> {
  const bookDetailsUrl = makeDetailsUrl(bookId)

  // Get book details
  const cachedDetails = detailsSessionCache.get(bookId)
  if (cachedDetails) {
    log.info(`Retrieving details for ${bookId} from cache`, 'fetchBookDetails')
    return cachedDetails
  } else {
    log.info(`Retrieving details for ${bookId} from URL`, 'fetchBookDetails')
    const rawDetails = await fetchJson<BookInfoDetailsMinimum>(bookDetailsUrl)
    rawDetails.dateAdded = new Date(rawDetails.dateAdded)
    const details = fillBookDetails(rawDetails)
    detailsSessionCache.set(bookId, details)
    return details
  }
}

/** Modify details to include default information when not provided by source,
 * e.g. URLs for cover images based on the ID.
 */
function fillBookDetails(detailsJson: BookInfoDetailsJson): BookInfoDetails {
  const coverUrl = detailsJson.coverUrl ?? makeCoverUrl(detailsJson.id)
  const coverThumbUrl = detailsJson.coverThumbUrl ?? makeCoverThumbUrl(detailsJson.id)
  return {...detailsJson, coverThumbUrl, coverUrl}
}
