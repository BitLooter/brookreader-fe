import { MOCK_BOOK_DETAILS_JSON, MOCK_BOOK_DETAILS_MINIMUM_JSON, MOCK_BOOK_ID } from '../testdata'
import { mockFetch, unmockFetch } from '../testutils'
import fetchBookDetails from './bookdetails'


afterEach(() => unmockFetch())

test('Loading details file', async () => {
  mockFetch(MOCK_BOOK_DETAILS_JSON)
  const details = await fetchBookDetails(MOCK_BOOK_ID)

  expect(details.id).toBe(MOCK_BOOK_ID)
})

test('Filling missing details with defaults', async () => {
  mockFetch(MOCK_BOOK_DETAILS_MINIMUM_JSON)
  const details = await fetchBookDetails(MOCK_BOOK_ID)

  expect(details.coverUrl).toBeDefined()
  expect(details.coverThumbUrl).toBeDefined()
})
