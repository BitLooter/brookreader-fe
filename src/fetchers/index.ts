/** Functions to retrieve data from the server */

import config from '../config'
import { CatalogStats } from '../models'

export { default as fetchCatalogIndex } from './catalogindex'
export { default as fetchBookDetails } from './bookdetails'


/** Fetch stats file */
export async function fetchStats(): Promise<CatalogStats> {
  const statsUrl = `${config.dataBaseUrl}/stats.json`
  const rawStats = await fetchJson<CatalogStats>(statsUrl)
  rawStats.server.date = new Date(rawStats.server.date)
  return rawStats
}

/** Wrapper for fetch that handles JSON parsing and errors */
export async function fetchJson<J>(url: string): Promise<J> {
  const response = await fetch(url)
  if (!response.ok) {
    throw new Error(`Cannot download JSON: ${response.status} ${response.statusText}`)
  }
  return response.json()
}
