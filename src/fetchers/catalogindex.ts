/** Manages book indexes */

import Ajv from 'ajv'

import bookshelfSchema from '../bookshelf.schema.json'
import config, { DEFAULT_ROOT_SHELF } from '../config'
import log from '../logger'
import { BookInfo, CatalogCollection, CatalogIndexCollectionsObject, CatalogIndex } from '../models'
import { fetchJson } from '.'


const ajv = new Ajv()
const validateBookshelf = ajv.compile(bookshelfSchema)
const indexCache: {[bookshelfURL: string]: Promise<CatalogIndex>} = {}

interface BookMetadataIndex {[bookId: string]: BookInfo}

// JSON data structures
interface BookshelfHeaderJson {
  name: string
}
interface BookJson {
  title: string
}
interface BookIndexJson {[bookId: string]: BookJson}
interface ShelfJson {
  name: string
  subshelves: string[]
  books: string[]
}
interface BookshelfJson {
  info: BookshelfHeaderJson
  shelves: {[shelfId: string]: ShelfJson}
  books: BookIndexJson
}

/** Loads the JSON book index at the URL asynchronously */
export default async function fetchBookIndex(
  url: string, rootShelf: string = DEFAULT_ROOT_SHELF
): Promise<CatalogIndex> {
  // Cache index so it's not redownloaded every time you close a book
  if (!(url in indexCache)) {
    log.info(`Downloading catalog index for ${url}`)
    indexCache[url] = indexFromUrl(url, rootShelf)
  } else {
    // May be called again before download is done, use cached promise
    log.info(`Using cached catalog index for ${url}`)
  }
  return indexCache[url]
}

/** Builds a catalog index from a downloaded bookshelf file */
async function indexFromUrl(url: string, rootShelf: string): Promise<CatalogIndex> {
  const bookshelf = await fetchJson<BookshelfJson>(url)

  if (!validateBookshelf(bookshelf)) {
    throw new Error('Bookshelf invalid: ' + ajv.errorsText(validateBookshelf.errors))
  }

  // Convert JSON data to internal bookshelf data
  const bookIndexEntries: BookMetadataIndex = {}
  for (const [bookId, bookData] of Object.entries(bookshelf.books)) {
    bookIndexEntries[bookId] = {
      ...bookData,
      // In the future cover URLs will be optional attributes in bookData and
      // these will become defaults when not present
      coverThumbUrl: `${config.coversBaseUrl}/${bookId}-thumb.png`,
      coverUrl: `${config.coversBaseUrl}/${bookId}-full.jpg`,
      id: bookId,
    }
  }
  const rootCollection = convertShelfToCollection(bookshelf, rootShelf, bookIndexEntries)

  return {
    collections: flattenCollection(rootCollection),
    name: bookshelf.info.name,
    rootCollection
  }
}

/** Transforms data from a JSON bookshelf into a tree of collections */
function convertShelfToCollection(bookshelf: BookshelfJson,
                                  shelfId: string,
                                  bookIndex: BookMetadataIndex,
                                  parent?: CatalogCollection,
                                  depth = 0,
                                  ): CatalogCollection {
  const currentShelf = bookshelf.shelves[shelfId]
  if (!currentShelf) {
    throw new Error(`Shelf ID ${shelfId} not found in bookshelf`)
  }

  const books = new Map<string, BookInfo>()
  for (const bookId of currentShelf.books) {
    books.set(bookId, bookIndex[bookId])
  }

  // Create object with placeholder collections so we can pass it as parent
  const thisCollection: CatalogCollection = {
    books,
    collections: [],
    count: -1,
    coverUrl: `${config.coversBaseUrl}/shelves/${shelfId}-thumb.png`,
    id: shelfId,
    name: currentShelf.name,
    parent
  }

  // Prevent recursive structures. Maximun depth was chosen somewhat
  // arbitrarily. May be changed or made configurable in the future but if it's
  // too deep you may want to restructure your data.
  // Not sure what the maximum is but Node doesn't throw a call stack error til
  // well over 1000, UX is the important factor here.
  if (depth >= 20) {
    log.warning(`Maxiumum collection depth reached for "${shelfId}". Do you
                 have a recursive shelf?`)
    thisCollection.collections = []
  } else {
    thisCollection.collections = currentShelf.subshelves.map( (subshelfId) =>
      convertShelfToCollection(
        bookshelf, subshelfId, bookIndex, thisCollection, depth = depth + 1) )
  }

  // Count is the total of all books in this and contained collections
  thisCollection.count = thisCollection.collections.reduce(
    (acc, cur) => acc + cur.count,
    currentShelf.books.length
  )

  return thisCollection
}

/**
 * Create a flat mapping of collection IDs to collections based on
 * IBookCollection tree generated from bookshelf file.
 */
function flattenCollection(thisCollection: CatalogCollection): CatalogIndexCollectionsObject {
  let flattened: CatalogIndexCollectionsObject = {}
  flattened[thisCollection.id] = thisCollection
  for (const sub of thisCollection.collections) {
    flattened = {...flattened, ...flattenCollection(sub)}
  }
  return flattened
}
