import { TEST_STATS } from '../../testdata'


// eslint-disable-next-line @typescript-eslint/no-explicit-any
const fetchers: any = jest.createMockFromModule('../index')

fetchers.fetchStats = async () => {
  return TEST_STATS
}

module.exports = fetchers
