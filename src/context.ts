/** Central location for React contexts */

import React from 'react'

import { ReaderBase } from './bookreader'
import { dummyReader } from './bookreader/dummyreader'
import { BarState } from './components/MainApp'
import { UserSettings } from './config'
import { BookInfoDetails } from './models'


// BookViewer
//////////////

export interface BookViewerContextValue {
  /** Closes current book and return to previous menu (Catalog, Home, etc.) */
  closeBook: () => void
  /** URL closeBook will return to */
  closeUrl: string
  /** Key (ID@source) of the book's collection */
  collectionKey: string
  /** Full details of current book */
  details: BookInfoDetails
  /** Size and direction of last page change */
  pageDelta: number
  /** Current page number of book */
  pageNum: number
  /** Reader for current book */
  reader: ReaderBase
  /** Jump to given page number */
  setPageNum: (pageNum: number) => void
}

export const BookViewerContext = React.createContext<BookViewerContextValue>({
  collectionKey: '',
  closeBook: () => null,
  closeUrl: '',
  details: {
    id: '',
    title: '',
    dateAdded: new Date(),
    url: '',
    coverThumbUrl: '',
    coverUrl: ''
  },
  pageDelta: 0,
  pageNum: 0,
  reader: dummyReader,
  setPageNum: () => null,
})


// TopBar
//////////

export interface TopBarContextValue {
  /** Set navigation information */
  setNav: (backUrl: string, title: string) => void
  /** Set whether overlaid over content or own block element */
  setOverlay: (overlay: boolean) => void
  /** Set visibility (for CSS, may have transitions) */
  setHidden: (hidden: boolean) => void
  /** Set all state at once */
  setup: (newState: BarState) => void
  /** Used to check that the bar's context is set */
  noProvider?: boolean
}

export const TopBarContext = React.createContext<TopBarContextValue>({
  setNav: () => null,
  setOverlay: () => null,
  setHidden: () => null,
  setup: () => null,
  noProvider: true
})


// UserSettings
////////////////

type ValidUserSettings = keyof UserSettings
export interface UserSettingsContextValue extends UserSettings {
  // Function overloading might be usable here somehow to associate names with
  // value types, when non-boolean settings are added. A previous experimental
  // implementation of the settings system allowed for this but was discarded
  // in favor of this context-based solution. All settings so far are booleans
  // so I haven't bothered implementing it here yet.
  updateSetting: (settingName: ValidUserSettings, settingValue: boolean) => void
}


export const UserSettingsContext = React.createContext<UserSettingsContextValue>({
  bookAnimatedTransitions: true,
  bookProgressVisible: true,
  updateSetting: () => null,
})
