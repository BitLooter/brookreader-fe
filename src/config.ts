/* Collects all configurable values in one place */
// TODO:BACKEND: Make this more configurable
import log from './logger'


/** The ID of the top-level shelf in the main bookshelf file */
export const DEFAULT_ROOT_SHELF = 'main'

const DATA_BASE_URL = process.env.REACT_APP_DATA_BASE_URL
const SITE_NAME = process.env.REACT_APP_SITE_NAME
const VERSION = process.env.REACT_APP_PACKAGE_VERSION

const DEFAULT_ABOUT_MESSAGE = `${SITE_NAME} is an instance of Brookreader, an
eBook reader that runs entirely in your web browser.`

export const SETTINGS_LOCALSTORAGE_KEY = 'settings'


// User settings
/////////////////

export interface UserSettings {
  /** If true animate page changes */
  bookAnimatedTransitions: boolean
  /** If true show the book progress at all times */
  bookProgressVisible: boolean
}

const DEFAULT_SETTINGS: UserSettings = {
  bookAnimatedTransitions: !matchMedia('(prefers-reduced-motion: reduce)').matches,
  bookProgressVisible: true
}

/** Load stored user settings and fill in with defaults when not available */
export function loadUserSettings(): UserSettings {
  const storedSettings = localStorage.getItem(SETTINGS_LOCALSTORAGE_KEY)
  const localSettings = storedSettings ? JSON.parse(storedSettings) : DEFAULT_SETTINGS
  return {...DEFAULT_SETTINGS, ...localSettings}
}

/** Save user settings to local storage */
export function saveUserSettings(settings: UserSettings): void {
  log.debug(`Saving settings to localStorage`, 'config')
  localStorage.setItem('settings', JSON.stringify(settings))
}


// Build configuration
///////////////////////

let error
if (!DATA_BASE_URL) {
  error = new Error("REACT_APP_DATA_BASE_URL is not defined")
} else if (!SITE_NAME) {
  error = new Error("REACT_APP_SITE_NAME is not defined")
} else if (!VERSION) {
  error = new Error("REACT_APP_PACKAGE_VERSION is not defined")
}

/** All configured items */
const config = {
  /** Information message displayed on about page */
  aboutMessage: process.env.REACT_APP_ABOUT_MESSAGE ?? DEFAULT_ABOUT_MESSAGE,
  /** Base URL for covers */
  coversBaseUrl: DATA_BASE_URL ? `${DATA_BASE_URL}/covers` : 'ERROR: dataBaseUrl not set',
  /** Base URL for Brookreader data */
  dataBaseUrl: DATA_BASE_URL ?? 'ERROR: dataBaseUrl not set',
  /** Base URL for details JSON */
  detailsBaseUrl: DATA_BASE_URL ? `${DATA_BASE_URL}/details` : 'ERROR: dataBaseUrl not set',
  /** Array of collection IDs to use for showcases. Showcases will be displayed
   * in the order listed. */
  enabledShowcases: ['featured', 'recent'],
  /** Name of the deployed site */
  siteName: SITE_NAME ?? 'ERROR: siteName not set',
  /** Frontend version */
  version: VERSION ?? 'ERROR: version not set',
  /** Problems with configuration reported here as an error object */
  error,
}

export default config
