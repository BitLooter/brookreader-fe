/** Utilities useful for tests */

import { render, RenderResult } from '@testing-library/react'
import React from 'react'
import { MemoryRouter } from 'react-router-dom'

import { BookViewerContext, BookViewerContextValue, TopBarContextValue, TopBarContext } from './context'
import { MOCK_BOOK_URL, MOCK_TOPBAR_CONTEXT, MOCK_VIEWER_CONTEXT } from './testdata'


/** Render JSX with a router */
export function renderWithRouter(component: JSX.Element, url = '/'): RenderResult {
  const routerComponent = <MemoryRouter initialEntries={[url]} initialIndex={0}>
    {component}
  </MemoryRouter>

  return render(routerComponent)
}

interface RenderWithTopBarOptions {
  url?: string
  context?: TopBarContextValue
}
/** Render JSX with a topbar context + requirements */
export function renderWithTopBar(
  component: JSX.Element,
  {url, context}: RenderWithTopBarOptions = {}
): RenderResult {
  url = url ? url : '/'
  context = context ? context : MOCK_TOPBAR_CONTEXT
  const topBarComponent = <TopBarContext.Provider value={context}>
    {component}
  </TopBarContext.Provider>

  return renderWithRouter(topBarComponent, url)
}

interface RenderWithBookOptions {
  url?: string
  context?: BookViewerContextValue
}
/** Render JSX with a book context + requirements */
export function renderWithBook(
  component: JSX.Element,
  {url, context}: RenderWithBookOptions = {}
): RenderResult {
  url = url ? url : MOCK_BOOK_URL
  context = context ? context : MOCK_VIEWER_CONTEXT
  const bookComponent = <BookViewerContext.Provider value={context}>
    {component}
  </BookViewerContext.Provider>

  return renderWithTopBar(bookComponent, {url})
}

const originalFetch = window.fetch
/** Replace fetch with a mock that returns data */
export function mockFetch(data?: string | Blob | ArrayBuffer): void {
  let length: number
  if (typeof data === 'string') {
    length = data.length
  } else if (data instanceof Blob) {
    length = data.size
  } else if (data instanceof ArrayBuffer) {
    length = data.byteLength
  } else {
    throw new Error('Cannot find data length')
  }

  const headers = {
    'Content-Length': length.toString()
  }

  window.fetch = jest.fn()
    .mockName('fetchMocked')
    .mockResolvedValue(new Response(data, {headers}))
}

/** Restore fetch to original native implementation */
export function unmockFetch(): void {
  window.fetch = originalFetch
}
