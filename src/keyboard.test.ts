import { fireEvent } from '@testing-library/dom'

import registerKeybindings from './keyboard'


test('Keyboard input', () => {
  let pressedA = false
  const bindings = {a: () => pressedA = true}
  registerKeybindings(bindings)
  fireEvent.keyDown(document, {key: 'z', code: 90})
  expect(pressedA).toBeFalsy()
  fireEvent.keyDown(document, {key: 'a', code: 65})
  expect(pressedA).toBeTruthy()
})
