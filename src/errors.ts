// Errors that can be raised by the application

/** Error loading a book source */
export class BookSourceError extends Error {}
/** Brookreader did something it's not supposed to do */
export class InternalError extends Error {}
/** Attempted reading a book before it's loaded */
export class NotLoadedError extends Error {}
